import { createRouter, createWebHistory, createWebHashHistory }  from 'vue-router'
import HomePage                                                  from './views/Home.vue'
import Login                                                     from './views/Login.vue'
import AdminEmpList                                              from './views/AdminPanelListEmpl.vue'
import UserDetail                                                from './views/UserDetails.vue'
import EmpAdd                                                    from './views/EmpAdd.vue'
import PatientList                                               from './views/Patient.vue'

import PatientInfo                                               from './views/PatientInfo.vue'
import PatientEdit                                               from './views/PatientEdit.vue'
import ExamsTable                                                from './views/Exams.vue'
import ExamDetails                                               from './components/ExamDetails.vue'

import DoctorsTable                                              from './views/Doctors.vue'
import DoctorDetail                                              from './views/DoctorDetail.vue'
import AssignedPatients                                          from './views/AssignedPatients.vue'
import DailyVisits                                               from './views/DailyVisits.vue'
import PatientControlPanel                                       from './views/PatientControlPanel.vue'

//import VisitControlPanel                                         from './views/VisitControlPanel.vue'
import VitalSignsTable                                           from './views/VitalSigns.vue'
import VitalSignDetail                                           from './views/VitalSignDetail.vue'
import Calendar                                                  from './components/home/Calendars.vue'
import Profile                                                   from './views/Profile.vue'
import MedicalOrdersTable                                        from './views/MedicalOrders.vue'
import VisitsTable                                               from './views/Visits.vue'
import VisitDetail                                              from './views/VisitDetail.vue'
import MedicalOrderDetail                                        from './views/MedicalOrderDetail.vue'
import NurseDetails                                              from "@/views/NurseDetaill";
import NurseTable                                                from './views/Nurses'
import ReferalTable                                              from "./views/Referal";
import ReferralsDetails                                          from "@/views/ReferralsDetails";
import TreatmentTable                                            from '@/views/Treatment'
import TreatmentDetail                                           from "@/views/TreatmentDetail";
import MedicalReceiptsTable                                      from './views/MedicalReceipts.vue'
import MedicalReceiptsDetails                                    from "./views/MedicalReceiptsDetails.vue";

const router = createRouter({
  history: process.env.IS_ELECTRON ? createWebHashHistory() : createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
    },
    {
      path: '/home',
      name: 'Home',
      component: HomePage,
    },
    {
      path: '/adminPanelListEmpl',
      name: 'adminEmpList',
      component: AdminEmpList,
    },
    {
      path: '/userDetails',
      name: 'UserDetails',
      component: UserDetail,
    } ,
    {
      path: '/empAdd',
      name: 'EmpAdd',
      component: EmpAdd,
    },
    {
      path: '/patients',
      name: 'PatientList',
      component: PatientList,
    },
    {
      path:'/patients/new',
      name:'PatientCreate',
      component:PatientInfo,
      props: true
    },
    {
      path:'/patientInfo/:id',
      name:'PatientInfo',
      component: PatientInfo,
      props: true
    },
    {
      path:'/patientEdit',
      name:'PatientEdit',
      component: PatientEdit,
    },
    {
      path: '/examsTable',
      name: 'ExamsTable',
      component: ExamsTable,
    },
    {
      path: '/examination',
      name: 'examination',
      component: ExamDetails,
      props: true
    },
    {
      path: '/examinations/new',
      name: 'examination',
      component: ExamDetails,
      props: true
    },
    {
       path: '/doctors',
       name: 'DoctorsTable',
       component: DoctorsTable
    },
    {
       path: '/doctor/:id',
       name: 'DoctorDetail',
       component: DoctorDetail,
       props: true
    },
    {
       path: '/doctor/new',
       name: 'DoctorCreate',
       component: DoctorDetail,
       props: true
    },
    {
      path: '/AssignedPatients',
      name: 'AssignedPatients',
      component: AssignedPatients,
   },
    {
        path: '/DailyVisits',
        name: 'DailyVisits',
        component: DailyVisits,
    },
    {
      path: '/PatientControlPanel',
      name: 'PatientControlPanel',
      component: PatientControlPanel,
    },
    {
       path: '/vital-signs',
       name: 'VitalSignsTable',
       component: VitalSignsTable
    },
    {
       path: '/vital-sign/:id',
       name: 'VitalSignDetail',
       component: VitalSignDetail,
       props: true
    },
    {
       path: '/vital-signs/new',
       name: 'VitalSignCreate',
       component: VitalSignDetail,
       props: true
    },
    {
      path: '/profile/calendar',
      name: 'Calendar',
      component: Calendar,
      props: true
    },
    {
      path: '/profile/me',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/medical-orders',
      name: 'MedicalOrdersTable',
      component: MedicalOrdersTable
    },
    {
    path: '/visits',
    name: 'VisitsTable',
    component: VisitsTable
    },
    {
      path: '/medical-order/:id',
      name: 'MedicalOrderDetail',
      component: MedicalOrderDetail,
      props: true
   },
   {
      path: '/medicalOrder/new',
      name: 'MedicalOrderCreate',
      component: MedicalOrderDetail,
      props: true
   },
   {
       path: '/nurses',
       name: 'NurseTable',
       component: NurseTable
   },
      {
          path: '/nurses/:id',
          name: 'NurseDetails',
          component: NurseDetails,
          props: true
      },
      {
          path:'/nurses/new',
          name:'NurseCreate',
          component: NurseDetails,
          props: true
      },
      {
        path: '/visit/:id',
        name: 'VisitDetail',
        component: VisitDetail,
        props: true
     },
     {
        path: '/visit/new',
        name: 'VisitCreate',
        component: VisitDetail,
        props: true
     },
    {
        path: '/referrals',
        name: 'ReferalTable',
        component: ReferalTable,
    },
    {
        path: '/referrals/new',
        name: 'ReferralsCreate',
        component: ReferralsDetails,
        props: true,
    },
    {
      path: '/referrals/:id',
      name: 'ReferralsDetails',
      component: ReferralsDetails,
      props: true,
    },
    {
      path: '/treatment',
      name: 'treatmentTabla',
      component: TreatmentTable,
    },
    {
      path: '/treatment/:id',
      name: 'TreatmentDedtails',
      component: TreatmentDetail,
      props: true
    },
    {
      path: '/treatment/new',
      name: 'TreatmentCreate',
      component: TreatmentDetail,
      props: true
    },
     {
         path: '/medicalReceiptsTable',
         name: 'MedicalReceiptsTable',
         component: MedicalReceiptsTable,
       },
       {
         path: '/medicalreceipt',
         name: 'medicalreceipt',
         component: MedicalReceiptsDetails,
         props: true
       },
       {
         path: '/medicalreceipts/new',
         name: 'medicalreceipt',
         component: MedicalReceiptsDetails,
         props: true
       },
    /*
    {
      path: '/VisitControlPanel',
      name: 'VisitControlPanel',
      component: VisitControlPanel,
    },*/
  ]
})

export default router
