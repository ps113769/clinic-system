import { createApp } from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'
import router from './router'
import store from "./store"
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import 'material-design-icons-iconfont/dist/material-design-icons.css'

loadFonts()

createApp(App)
  .use(router)
  .use(vuetify)
  .use(store)
  .mount('#app')