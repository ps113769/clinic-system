import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class TreatmentService {
    getTreatmentsList() {
        return axios.get(API_URL + 'treatments', { headers: authHeader() })
            .then(response => {
                return response;
            });
    }
    // getTreatmentsSelectList() {
    //     return axios.get(API_URL + 'treatments/select', { headers: authHeader() })
    //         .then(response => {
    //             return response;
    //         });
    // }
    getTreatment(id) {
        return axios.get(API_URL + `treatment/${id}`, { headers: authHeader() })
            .then(response => {
                return response;
            });
    }
    createTreatment (treatment) {
        return axios.post(API_URL + `treatment`, treatment, { headers: authHeader() })
    }
    updateTreatment (id, treatment) {
        return axios.put(API_URL + `treatment/${id}`, treatment, { headers: authHeader() })
    }

}
export default new TreatmentService();