import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class NurseService {
  getNurseList() {
    return axios.get(API_URL + 'nurses', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getNurseSelectList() {
    return axios.get(API_URL + 'nurses/select', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getNurse(id) {
    return axios.get(API_URL + `nurse/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createNurse (nurse) {
    return axios.post(API_URL + `nurse`, nurse, { headers: authHeader() })
  }
  updateNurse (id, nurse) {
    return axios.put(API_URL + `nurse/${id}`, nurse, { headers: authHeader() })
  }
  deleteNurse(id) {
    return axios.delete(API_URL + `nurse/${id}`, { headers: authHeader() })
  }
}
export default new NurseService();