import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class VitalSignService {
  getList() {
    return axios.get(API_URL + 'vital-signs', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getVitalSign(id) {
    return axios.get(API_URL + `vital-sign/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createVitalSign (vitalSign) {
    return axios.post(API_URL + `vital-sign`, vitalSign, { headers: authHeader() })
  }
  updateVitalSign (id, vitalSign) {
    return axios.put(API_URL + `vital-sign/${id}`, vitalSign, { headers: authHeader() })
  }
  deleteVitalSign (id) {
    return axios.delete(API_URL + `vital-sign/${id}`, { headers: authHeader() })
  }
}
export default new VitalSignService();