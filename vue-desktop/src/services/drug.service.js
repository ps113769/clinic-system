import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class DrugService {
  getDrugList() {
    return axios.get(API_URL + 'drugs', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getDrugByName(name) {
    return axios.get(API_URL + `drug/${name}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
}
export default new DrugService();