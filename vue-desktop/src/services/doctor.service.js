import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class DoctorService {
  getDoctorsList() {
    return axios.get(API_URL + 'doctors', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getDoctorSelectList() {
    return axios.get(API_URL + 'doctors/select', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getDoctor(id) {
    return axios.get(API_URL + `doctor/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createDoctor (doctor) {
    return axios.post(API_URL + `doctor`, doctor, { headers: authHeader() })
  }
  updateDoctor (id, doctor) {
    return axios.put(API_URL + `doctor/${id}`, doctor, { headers: authHeader() })
  }
  deleteDoctor (id) {
    return axios.delete(API_URL + `doctor/${id}`, { headers: authHeader() })
  }

}
export default new DoctorService();