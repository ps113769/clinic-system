import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class VisitService {
  getVisitsList() {
    return axios.get(API_URL + 'visits', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getVisitsTodayList() {
    return axios.get(API_URL + 'visits/today', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getVisit(id) {
    return axios.get(API_URL + `visit/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createVisit (visit) {
    return axios.post(API_URL + `visit`, visit, { headers: authHeader() })
  }
  updateVisit (id, visit) {
    return axios.put(API_URL + `visit/${id}`, visit, { headers: authHeader() })
  }
  deleteVisit (id) {
    return axios.delete(API_URL + `visit/${id}`, { headers: authHeader() })
  }
}
export default new VisitService();