import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class ReferralService {
    getReferralsList() {
        return axios.get(API_URL + 'referrals', { headers: authHeader() })
            .then(response => {
                return response;
            });
    }
    getReferralsSelectList() {
        return axios.get(API_URL + 'referrals/select', { headers: authHeader() })
            .then(response => {
                return response;
            });
    }
    getReferrals(id) {
        return axios.get(API_URL + `referral/${id}`, { headers: authHeader() })
            .then(response => {
                return response;
            });
    }
    createReferrals(referal) {
        return axios.post(API_URL + `referral`, referal, { headers: authHeader() })
    }
    updateReferrals (id, referal) {
        return axios.put(API_URL + `referral/${id}`, referal, { headers: authHeader() })
    }

}
export default new ReferralService();