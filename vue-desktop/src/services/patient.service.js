import axios      from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class PatientService {
  getPatientList() {
    return axios.get(API_URL + 'patients', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getPatientFullNameList() {
    return axios.get(API_URL + 'patients/fullname', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getPatient(id) {
    return axios.get(API_URL + `patient/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createPatient(patient) {
    return axios.post(API_URL + 'patient', patient, { headers: authHeader() })
  }
  updatePatient(id, patient){
    return axios.put(API_URL + `patient/${id}`, patient, { headers: authHeader() } )
  }
  deletePatient(id) {
    return axios.delete(API_URL + `patient/${id}`, { headers: authHeader() })
  }
}
export default new PatientService();