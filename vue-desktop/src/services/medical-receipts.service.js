import axios      from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class MedicalReceiptsService {
  getMedicalReceiptsList() {
    return axios.get(API_URL + 'medical-receipts', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getMedicalReceipt(id) {
    return axios.get(API_URL + `medical-receipt/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  generate(id) {
    return axios.get(API_URL + `medical-receipt/generate/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createMedicalReceipt(medicalreceipt) {
    return axios.post(API_URL + 'medical-receipt', medicalreceipt, { headers: authHeader() })
  }
  updateMedicalReceipt(id, medicalreceipt){
    return axios.put(API_URL + `medical-receipt/${id}`, medicalreceipt, { headers: authHeader() } )
  }
  deleteMedicalReceipts(id) {
    return axios.delete(API_URL + `medicalreceipt/${id}`, { headers: authHeader() })
  }
}
export default new MedicalReceiptsService();