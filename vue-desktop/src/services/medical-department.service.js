import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class MedicalDepartmentService {
  getMedicalDepartmentList() {
    return axios.get(API_URL + 'medical-departments', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getMedicalDepartmentByName(name) {
    return axios.get(API_URL + `medical-department/${name}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
}
export default new MedicalDepartmentService();