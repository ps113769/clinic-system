import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/auth/'
class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'login', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data))
          this.me()
        }
        return response.data;
      });
  }
  logout() {
    localStorage.removeItem('user');
  }
  me() {
    return axios.get(API_URL + 'me', { headers: authHeader() })
    .then(response => {
        if(response.data.firstName){
            console.log(response.data)
            localStorage.setItem('me', JSON.stringify(response.data))
        }
        return response.data;
    });
  }
}
export default new AuthService();