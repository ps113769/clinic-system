import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class SpecializationService {
  getSpecializationList() {
    return axios.get(API_URL + 'specializations', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getSpecializationByName(name) {
    return axios.get(API_URL + `specialization/${name}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
}
export default new SpecializationService();