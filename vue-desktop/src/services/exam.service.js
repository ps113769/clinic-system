import axios      from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class ExamService {
  getExamList() {
    return axios.get(API_URL + 'exams', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getExam(id) {
    return axios.get(API_URL + `exam/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createExam(exam) {
    return axios.post(API_URL + 'exam', exam, { headers: authHeader() })
  }
  updateExam(id, exam){
    return axios.put(API_URL + `exam/${id}`, exam, { headers: authHeader() } )
  }
  deleteExam(id) {
    return axios.delete(API_URL + `exam/${id}`, { headers: authHeader() })
  }
}
export default new ExamService();