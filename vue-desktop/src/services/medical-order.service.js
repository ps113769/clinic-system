import axios from 'axios'
import authHeader from './auth-header'
const API_URL = 'http://localhost:8080/api/'
class MedicalOrdersService {
  getMedicalOrdersList() {
    return axios.get(API_URL + 'medical-orders', { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  getMedicalOrder(id) {
    return axios.get(API_URL + `medical-order/${id}`, { headers: authHeader() })
    .then(response => {
        return response;
    });
  }
  createMedicalOrder (medicalOrder) {
    return axios.post(API_URL + `medical-order`, medicalOrder, { headers: authHeader() })
  }
  updateMedicalOrder (id, medicalOrder) {
    return axios.put(API_URL + `medical-order/${id}`, medicalOrder, { headers: authHeader() })
  }
  deleteMedicalOrder (id) {
    return axios.delete(API_URL + `medical-order/${id}`, { headers: authHeader() })
  }
}
export default new MedicalOrdersService();