package org.byleco.api.repository;

import org.byleco.api.model.MedicalReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalReceiptRepository extends JpaRepository<MedicalReceipt, Long> {
    List<MedicalReceipt> findByPatientId(Long patientId);
}
