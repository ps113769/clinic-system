package org.byleco.api.repository;

import org.byleco.api.model.MedicalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MedicalDepartmentRepository extends JpaRepository<MedicalDepartment, Long> {
    Optional<MedicalDepartment> findById(String name);
}
