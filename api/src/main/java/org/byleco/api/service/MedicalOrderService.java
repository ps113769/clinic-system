package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.*;
import org.byleco.api.payload.doctor.DoctorRequest;
import org.byleco.api.payload.medicalOrder.MedicalOrderRequest;
import org.byleco.api.repository.MedicalOrderRepository;
import org.byleco.api.repository.NurseRepository;
import org.byleco.api.repository.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicalOrderService {
    private final MedicalOrderRepository medicalOrderRepository;
    private final PatientRepository patientRepository;
    private final NurseRepository nurseRepository;

    /**
     * Get all the medical orders from the database and return them as a list.
     *
     * @return A list of all medical orders
     */
    public List<MedicalOrder> getMedicalOrders() {
        return medicalOrderRepository.findAll();
    }

    /**
     * If the medical order exists, return it, otherwise throw an exception.
     *
     * @param id The id of the medical order to be fetched.
     * @return A Medical Order object
     */
    public MedicalOrder fetchMedicalOrder(Long id) {
        return medicalOrderRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Medical Order not found"));
    }

    /**
     * > Create a new medical order for a patient, and assign it to a list of nurses
     *
     * @param request The request object that contains the data that the user sent to the API.
     * @return A MedicalOrder object
     */
    public MedicalOrder createMedicalOrder(MedicalOrderRequest request) {
        Patient patient = patientRepository.findById(request.getPatient_id()).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));

        List<Nurse> nurses = new ArrayList<>();

        for (Long nurse_id: request.getNurses()) {
            nurses.add(nurseRepository.findById(nurse_id).orElseThrow(() -> new ResourceNotFoundException("Nurse not found")));
        }

        MedicalOrder medicalOrder = MedicalOrder.builder()
                .name(request.getName())
                .description(request.getDescription())
                .patient(patient)
                .nurse(nurses)
                .build();

        return medicalOrderRepository.save(medicalOrder);
    }

    /**
     * It updates a medical order by id, and it takes in a request object that contains the name, description, patient_id,
     * and a list of nurse_ids
     *
     * @param id The id of the medical order to be updated
     * @param request The request body of the request.
     * @return MedicalOrder
     */
    public MedicalOrder updateMedicalOrder(Long id, MedicalOrderRequest request) {
        Patient patient = patientRepository.findById(request.getPatient_id()).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));

        List<Nurse> nurses = new ArrayList<>();

        for (Long nurse_id: request.getNurses()) {
            nurses.add(nurseRepository.findById(nurse_id).orElseThrow(() -> new ResourceNotFoundException("Nurse not found")));
        }

        MedicalOrder medicalOrder = medicalOrderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Medical Order not found"));

        medicalOrder.setName(request.getName());
        medicalOrder.setDescription(request.getDescription());
        medicalOrder.setPatient(patient);
        medicalOrder.setNurse(nurses);

        return medicalOrderRepository.save(medicalOrder);
    }

    /**
     * Delete a medical order by id.
     *
     * @param id The id of the medical order to be deleted.
     */
    public void deleteMedicalOrder(Long id) {
        medicalOrderRepository.deleteById(id);
    }
}
