package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.*;
import org.byleco.api.payload.vital.VitalSignRequest;
import org.byleco.api.repository.PatientRepository;
import org.byleco.api.repository.VitalSignRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VitalSignService {
    private final VitalSignRepository vitalSignRepository;
    private final PatientRepository patientRepository;
    private final UserService userService;

    /**
     * Get all the vital signs from the database and return them as a list.
     *
     * @return A list of all the vital signs in the database.
     */
    public List<VitalSign> getVitalSigns() {
        return vitalSignRepository.findAll();
    }

    /**
     * If the vital sign exists, return it, otherwise throw an exception.
     *
     * @param id The id of the VitalSign to be fetched.
     * @return A VitalSign object
     */
    public VitalSign fetchVitalSign(Long id) {
        return vitalSignRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Vital Sign not found"));
    }

    /**
     * > This function creates a new vital sign for a patient
     *
     * @param request The request object that contains the parameters that are passed to the API.
     * @return A VitalSign object is being returned.
     */
    public VitalSign createVitalSign(VitalSignRequest request) {
        Patient patient = patientRepository.findById(request.getPatient_id()).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        User medicalUser = userService.findById(request.getMedicalUser_id());

        VitalSign vitalSign = VitalSign.builder()
                .name(request.getName())
                .description(request.getDescription())
                .medicalUser(medicalUser)
                .patient(patient)
                .build();

        return vitalSignRepository.save(vitalSign);
    }

    /**
     * It updates a vital sign by id, and returns the updated vital sign
     *
     * @param id The id of the VitalSign object to be updated.
     * @param request The request object that contains the data that will be used to update the vital sign.
     * @return A VitalSign object is being returned.
     */
    public VitalSign updateVitalSign(Long id, VitalSignRequest request) {
        VitalSign vitalSign = vitalSignRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Vital Sign not found"));
        Patient patient = patientRepository.findById(request.getPatient_id()).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        User medicalUser = userService.findById(request.getMedicalUser_id());

        vitalSign.setName(request.getName());
        vitalSign.setDescription(request.getDescription());
        vitalSign.setPatient(patient);
        vitalSign.setMedicalUser(medicalUser);

        return vitalSignRepository.save(vitalSign);
    }

    /**
     * > Delete a vital sign by id
     *
     * @param id The id of the VitalSign object to be deleted.
     */
    public void deleteVitalSign(Long id) {
        vitalSignRepository.deleteById(id);
    }
}
