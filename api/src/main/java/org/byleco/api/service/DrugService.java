package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Drug;
import org.byleco.api.model.Specialization;
import org.byleco.api.repository.DrugRepository;
import org.byleco.api.repository.SpecializationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DrugService {
    private final DrugRepository drugRepository;

    /**
     * Get all the drugs from the database and return them as a list.
     *
     * @return A list of all drugs in the database.
     */
    public List<Drug> getDrugs() {
        return drugRepository.findAll();
    }

    /**
     * If the drug exists, return it, otherwise throw an exception.
     *
     * @param id The id of the drug to be retrieved.
     * @return A drug object
     */
    public Drug getById(Long id) {
        return drugRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Drug not found"));
    }

    /**
     * Find all drugs by name, or throw an exception if any of them are not found.
     *
     * @param name The name of the drug.
     * @return A list of drugs
     */
    public List<Drug> findAllByName(List<String> name) {
        return name.stream()
                .map(drugName -> drugRepository.findByName(drugName).orElseThrow(() -> new ResourceNotFoundException("Drug " + drugName + " not found!")))
                .collect(Collectors.toList());
    }
}
