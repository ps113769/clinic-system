package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Patient;
import org.byleco.api.payload.patient.PatientRequest;
import org.byleco.api.payload.patient.PatientResponse;
import org.byleco.api.repository.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final PatientRepository patientRepository;

    /**
     * Get all patients from the database and return them as a list.
     *
     * @return A list of all patients in the database.
     */
    public List<Patient> getPatients() {
        return patientRepository.findAll();
    }

    /**
     * We are getting all the patients from the database, then we are creating a new list of PatientResponse objects, then
     * we are iterating through the list of patients and creating a new PatientResponse object for each patient, then we
     * are adding the PatientResponse object to the list of PatientResponse objects, then we are returning the list of
     * PatientResponse objects
     *
     * @return A list of PatientResponse objects.
     */
    public List<PatientResponse> getPatientsFullName() {
        List<Patient> patients = patientRepository.findAll();
        List<PatientResponse> response = new ArrayList<>();

        for (Patient patient: patients
             ) {
            PatientResponse patientResponse = new PatientResponse();
            patientResponse.setName(patient.getName());
            patientResponse.setId(patient.getId());
            patientResponse.setSurname(patient.getSurname());
            patientResponse.setPhone(patient.getPhone());
            patientResponse.setAddress(patient.getAddress());
            patientResponse.setCity(patient.getCity());
            patientResponse.setState(patient.getState());
            patientResponse.setZip(patient.getZip());
            patientResponse.setCountry(patient.getCountry());
            patientResponse.setFullName(patient.getName() + " "+ patient.getSurname());

            response.add(patientResponse);
        }
        return response;
    }

    /**
     * We create a new patient object using the builder pattern, and then save it to the database
     *
     * @param patient The patient object that is being created.
     * @return A new patient object is being returned.
     */
    public Patient createPatient(PatientRequest patient) {
        Patient newPatient = Patient.builder()
                .name(patient.getName())
                .surname(patient.getSurname())
                .phone(patient.getPhone())
                .address(patient.getAddress())
                .city(patient.getCity())
                .state(patient.getState())
                .zip(patient.getZip())
                .country(patient.getCountry())
                .build();
        return patientRepository.save(newPatient);
    }

    /**
     * Delete a patient by id.
     *
     * @param id The id of the patient to be deleted.
     */
    public void deletePatient(Long id) {
        patientRepository.deleteById(id);
    }

    /**
     * If the patient exists, return it, otherwise throw an exception.
     *
     * @param patientId The id of the patient to be retrieved.
     * @return A patient object
     */
    public Patient getPatient(Long patientId) {
        return patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
    }

    /**
     * Update the patient with the given id with the given request.
     *
     * @param patientId The id of the patient to be updated.
     * @param request The request object that contains the data that the user has entered in the form.
     * @return Patient
     */
    public Patient updatePatient(Long patientId, PatientRequest request) {
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));

        patient.setName(request.getName());
        patient.setSurname(request.getSurname());
        patient.setPhone(request.getPhone());
        patient.setAddress(request.getAddress());
        patient.setCity(request.getCity());
        patient.setState(request.getState());
        patient.setZip(request.getZip());
        patient.setCountry(request.getCountry());

        return patientRepository.save(patient);
    }
}
