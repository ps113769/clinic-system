package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.*;
import org.byleco.api.repository.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class SeedDatabase {
    private final DoctorRepository doctorRepository;
    private final PatientRepository patientRepository;
    private final DrugRepository drugRepository;
    private final ExaminationRepository examinationRepository;
    private final MedicalDepartmentRepository medicalDepartmentRepository;
    private final MedicalOrderRepository medicalOrderRepository;
    private final NurseRepository nurseRepository;
    private final ReferralRepository referralRepository;
    private final RoleRepository roleRepository;
    private final SpecializationRepository specializationRepository;
    private final TreatmentRepository treatmentRepository;
    private final UserRepository userRepository;
    private final VisitRepository visitRepository;
    private final VitalSignRepository vitalSignRepository;
    private final PasswordEncoder passwordEncoder;
    private final MedicalReceiptRepository medicalReceiptRepository;

    @PostConstruct
    private void createDatabase() {
        MedicalDepartment medicalDepartment1 = MedicalDepartment.builder()
                .name("Chirurgia")
                .description("Chirurgia")
                .build();
        MedicalDepartment medicalDepartment2 = MedicalDepartment.builder()
                .name("Onkologia")
                .description("Onkologia")
                .build();
        MedicalDepartment medicalDepartment3 = MedicalDepartment.builder()
                .name("POZ")
                .description("POZ")
                .build();

        medicalDepartmentRepository.save(medicalDepartment1);
        medicalDepartmentRepository.save(medicalDepartment2);
        medicalDepartmentRepository.save(medicalDepartment3);


        Specialization specialization1 = Specialization.builder()
                .name("Chirurgia")
                .description("Chirurgia")
                .build();
        Specialization specialization2 = Specialization.builder()
                .name("Onkologia")
                .description("Onkologia")
                .build();
        Specialization specialization3 = Specialization.builder()
                .name("Rodzinna")
                .description("Rodzinna")
                .build();

        Specialization save1 = specializationRepository.save(specialization1);
        Specialization save2 = specializationRepository.save(specialization2);
        Specialization save3 = specializationRepository.save(specialization3);

        Doctor doctor1 = Doctor.builder()
                .firstName("Piotr")
                .lastName("Nowak")
                .username("pnowak")
                .password(passwordEncoder.encode("admin123"))
                .specializations(Collections.singletonList(save1))
                .medicalDepartment(medicalDepartment1)
                .build();
        Doctor doctor2 = Doctor.builder()
                .firstName("Adam")
                .lastName("Kowal")
                .username("akowal")
                .specializations(Collections.singletonList(save2))
                .medicalDepartment(medicalDepartment2)
                .password(passwordEncoder.encode("admin123"))
                .build();
        Doctor doctor3 = Doctor.builder()
                .firstName("Sebastian")
                .lastName("Paśko")
                .username("spasko")
                .medicalDepartment(medicalDepartment3)
                .specializations(Collections.singletonList(save3))
                .password(passwordEncoder.encode("admin123"))
                .build();

        doctorRepository.save(doctor1);
        doctorRepository.save(doctor2);
        doctorRepository.save(doctor3);

        Nurse nurse1 = Nurse.builder()
                .firstName("Anna")
                .lastName("Kowal")
                .username("akowal")
                .password(passwordEncoder.encode("admin123"))
                .medicalDepartment(medicalDepartment1)
                .build();

        Nurse nurse2 = Nurse.builder()
                .firstName("Julka")
                .lastName("Nowak")
                .username("jnowak")
                .password(passwordEncoder.encode("admin123"))
                .medicalDepartment(medicalDepartment2)
                .build();
        Nurse nurse3 = Nurse.builder()
                .firstName("Grażyna")
                .lastName("Bąk")
                .username("gbak")
                .password(passwordEncoder.encode("admin123"))
                .medicalDepartment(medicalDepartment3)
                .build();

        nurseRepository.save(nurse1);
        nurseRepository.save(nurse2);
        nurseRepository.save(nurse3);

        Patient patient1 = Patient.builder()
                .name("Piotr")
                .surname("Nowak")
                .phone("123456789")
                .address("ul. Lwowska 5")
                .city("Rzeszów")
                .state("Podkarpacie")
                .zip("35-000")
                .country("Polska")
                .build();
        Patient patient2 = Patient.builder()
                .name("Sebastian")
                .surname("Nowak")
                .phone("987654321")
                .address("ul. Rejtana 10")
                .city("Rzeszów")
                .state("Podkarpacie")
                .zip("35-000")
                .country("Polska")
                .build();
        Patient patient3 = Patient.builder()
                .name("Adam")
                .surname("Mocha")
                .phone("135798642")
                .address("ul. Warszawska 32a")
                .city("Rzeszów")
                .state("Podkarpacie")
                .zip("35-000")
                .country("Polska")
                .build();
        Patient patient4 = Patient.builder()
                .name("Marcin")
                .surname("Saja")
                .phone("111111111")
                .address("ul. Podkarpacka 1")
                .city("Rzeszów")
                .state("Podkarpacie")
                .zip("35-000")
                .country("Polska")
                .build();
        Patient patient5 = Patient.builder()
                .name("Krzysztof")
                .surname("Płaza")
                .phone("222222222")
                .address("ul. Litewska 123")
                .city("Rzeszów")
                .state("Podkarpacie")
                .zip("35-000")
                .country("Polska")
                .build();

        patientRepository.save(patient1);
        patientRepository.save(patient2);
        patientRepository.save(patient3);
        patientRepository.save(patient4);
        patientRepository.save(patient5);

        Drug drug1 = Drug.builder()
                .name("Paracetamol")
                .description("Lek przeciwgoraczkowy")
                .build();
        Drug drug2 = Drug.builder()
                .name("Acard")
                .description("Preparat hamujący agregację płytek krwi.")
                .build();
        Drug drug3 = Drug.builder()
                .name("Allegra")
                .description("Lek przeciwhistaminowy o działaniu przeciwalergicznym.")
                .build();
        Drug drug4 = Drug.builder()
                .name("Atorvox")
                .description("Lek zmniejszający stężenie lipidów we krwi.")
                .build();
        Drug drug5 = Drug.builder()
                .name("Azzalure")
                .description("Neurotoksyna porażająca przewodnictwo nerwowo-mięśniowe")
                .build();

        drugRepository.save(drug1);
        drugRepository.save(drug2);
        drugRepository.save(drug3);
        drugRepository.save(drug4);
        drugRepository.save(drug5);

        LocalDate data = LocalDate.parse("2022-05-10");

        Examination examination1 = Examination.builder()
                .name("Badanie moczu")
                .description("Badanie polegajace na analizie próbki moczu pacjenta")
                .patient(patient1)
                .date(data)
                .result("Badanie wykazalo poprawne wartość moczu")
                .realized(true)
                .build();
        Examination examination2 = Examination.builder()
                .name("Badanie krwi")
                .description("Badanie polegajace na analizie próbki krwi pacjenta")
                .date(data)
                .patient(patient2)
                .result("Badanie wykazalo zanizona wartość płytek we krwi")
                .realized(false)
                .build();
        Examination examination3 = Examination.builder()
                .name("Badanie kału")
                .description("Badanie polegajace na analizie próbki kału pacjenta")
                .date(data)
                .patient(patient3)
                .result("Próbka kału nie zawiera elementów nieporządanych")
                .realized(false)
                .build();
        Examination examination4 = Examination.builder()
                .name("Badanie krwi")
                .description("Badanie polegajace na analizie próbki krwi")
                .date(data)
                .patient(patient5)
                .result("Wyniki w normie")
                .realized(true)
                .build();

        examinationRepository.save(examination1);
        examinationRepository.save(examination2);
        examinationRepository.save(examination3);
        examinationRepository.save(examination4);

        Visit visit1 = Visit.builder()
                .diagnosis("Diagnoza")
                .medicalInterview("Wywiad")
                .date(data)
                .doctor(doctor1)
                .patient(patient1)
                .medicalPrescription(null)
                .build();

        Visit visit2= Visit.builder()
                .diagnosis("Diagnoza")
                .medicalInterview("Wywiad")
                .date(data)
                .doctor(doctor2)
                .patient(patient2)
                .medicalPrescription(null)
                .build();

        Visit visit3 = Visit.builder()
                .diagnosis("Diagnoza")
                .medicalInterview("Wywiad")
                .date(data)
                .doctor(doctor3)
                .patient(patient3)
                .medicalPrescription(null)
                .build();

        visitRepository.save( visit1);
        visitRepository.save( visit2);
        visitRepository.save( visit3);

        VitalSign vitalSign1 = VitalSign.builder()
                .name("Tętno")
                .description("Parametr poprawny")
                .medicalUser(nurse1)
                .patient(patient1)
                .build();

        VitalSign vitalSign2 = VitalSign.builder()
                .name("Ciśnienie krwi")
                .description("Parametr poprawny")
                .medicalUser(nurse2)
                .patient(patient2)
                .build();

        VitalSign vitalSign3 = VitalSign.builder()
                .name("Poziom tlenu")
                .description("Parametr poprawny")
                .medicalUser(nurse3)
                .patient(patient3)
                .build();

        vitalSignRepository.save(vitalSign1);
        vitalSignRepository.save(vitalSign2);
        vitalSignRepository.save(vitalSign3);

        List<Drug> drugList1 = new ArrayList<>();
        drugList1.add(drug1);
        drugList1.add(drug2);
        drugList1.add(drug3);

        MedicalReceipt medicalReceipt1 = MedicalReceipt.builder()
                .drugs(drugList1)
                .patient(patient1)
                .expirationDate(LocalDate.now())
                .build();

        medicalReceiptRepository.save(medicalReceipt1);

        List<Nurse> nurseList1 = new ArrayList<>();
        nurseList1.add(nurse1);

        MedicalOrder medicalOrder1 = MedicalOrder.builder()
                .name("Pobranie krwi")
                .nurse(nurseList1)
                .description("Należy pobrać krew do badań")
                .patient(patient1)
                .build();

        List<Nurse> nurseList2 = new ArrayList<>();
        nurseList2.add(nurse2);

        MedicalOrder medicalOrder2 = MedicalOrder.builder()
                .name("Zastrzyk do mięsniowy")
                .nurse(nurseList2)
                .description("Należy wykonać zastrzyk do mięśniowy")
                .patient(patient2)
                .build();

        medicalOrderRepository.save(medicalOrder1);
        medicalOrderRepository.save(medicalOrder2);

        Referral referral1 = Referral.builder()
                .createdAt(LocalDateTime.now())
                .patient(patient1)
                .doctor(doctor1)
                .isChecked(false)
                .description("Skierowanie do poradni")
                .status(TypeOfReferral.SPECIALIST_CLINIC)
                .build();

        Referral referral2 = Referral.builder()
                .createdAt(LocalDateTime.now())
                .patient(patient2)
                .doctor(doctor2)
                .isChecked(false)
                .description("Skierowanie do poradni")
                .status(TypeOfReferral.SPECIALIST_CLINIC)
                .build();

        Referral referral3 = Referral.builder()
                .createdAt(LocalDateTime.now())
                .patient(patient3)
                .doctor(doctor3)
                .isChecked(false)
                .description("Skierowanie na badanie")
                .status(TypeOfReferral.MEDICAL_EXAMINATION)
                .build();

        referralRepository.save(referral1);
        referralRepository.save(referral2);
        referralRepository.save(referral3);

        Treatment treatment1 = Treatment.builder()
                .numberOfRoom(1)
                .patient(patient1)
                .doctor(doctor1)
                .date(LocalDate.now())
                .diagnosis("Kolano kinomana")
                .medicalInterview("Pacjent skarżył się na dolegliwości związane z bólem kolana")
                .build();

        Treatment treatment2 = Treatment.builder()
                .numberOfRoom(1)
                .patient(patient2)
                .doctor(doctor2)
                .date(LocalDate.now())
                .diagnosis("Biodro golfisty")
                .medicalInterview("Pacjent skarżył się na dolegliwości związane z silnym bólem biodra")
                .build();

        treatmentRepository.save(treatment1);
        treatmentRepository.save(treatment2);
    }
}
