package org.byleco.api.service;

import com.itextpdf.io.IOException;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.extgstate.PdfExtGState;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Drug;
import org.byleco.api.model.MedicalReceipt;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class PdfGenerator {
    public static final String DEST = "/receipts/";

    /**
     * It takes a MedicalReceipt object, generates a random UUID, creates a PDF file with the UUID as a name, and then
     * fills the PDF with the data from the MedicalReceipt object
     *
     * @param medicalReceipt object of type MedicalReceipt, which contains all the data needed to generate a PDF file.
     * @return Path to the generated PDF file.
     */
    public String GeneratePdf(MedicalReceipt medicalReceipt) throws Exception {
        Path currentRelativePath = Paths.get("");
        String currentPath = currentRelativePath.toAbsolutePath().toString();

        File directory = new File(currentPath + "/receipts");
        if (!directory.exists()) {
            directory.mkdir();
        }

        UUID uuid = UUID.randomUUID();
        String path = currentPath + DEST + uuid + ".pdf";
        Document document;
        try {
            PdfDocument pdf = new PdfDocument(new PdfWriter(path));
            //pdf size
            PageSize ps = PageSize.A5;
            pdf.setDefaultPageSize(ps);
            //document
            document = new Document(pdf);
            document.add(new Paragraph("Data wystawienia:" + medicalReceipt.getExpirationDate().toString()).setFontSize(8)
                    .setTextAlignment(TextAlignment.RIGHT));

            Paragraph title = new Paragraph("RECEPTA")
                    .setTextAlignment(TextAlignment.CENTER)
                    .setFontSize(16);
            document.add(title);
            document.add(new Paragraph("Imie i nazwisko: " + medicalReceipt.getPatient().getName() + " " + medicalReceipt.getPatient().getSurname()).setFontSize(12).setMarginTop(10));
            document.add(new Paragraph("Kod recepty: " + uuid).setFontSize(12));
            document.add(new Paragraph("Lista leków:").setFontSize(12).setMarginTop(20));
            Table table = new Table(UnitValue.createPercentArray(new float[3])).useAllAvailableWidth();

            table.addHeaderCell("Lp.").setFontSize(8);
            table.addHeaderCell("Nazwa leku").setFontSize(8);
            table.addHeaderCell("Opis").setFontSize(8);

            List<Drug> drugs = medicalReceipt.getDrugs();
            int lp = 1;

            for (Drug drug : drugs
            ) {
                table.addCell(String.valueOf(lp)).setFontSize(8);
                table.addCell(drug.getName()).setFontSize(8);
                table.addCell(drug.getDescription()).setFontSize(8);
                lp++;
            }
            document.add(table);
            document.close();

        } catch (Exception e) {
            throw new Exception("PDF Generator error");
        }
        return path;
    }
}
