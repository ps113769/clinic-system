package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.*;
import org.byleco.api.repository.*;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VisitService {
    private final VisitRepository visitRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final MedicalReceiptRepository medicalReceiptRepository;

    /**
     * Get all the visits from the database and return them as a list.
     *
     * @return A list of all the visits in the database.
     */
    public List<Visit> getVisits() {
        return visitRepository.findAll();
    }

    /**
     * If the visit exists, return it, otherwise throw an exception.
     *
     * @param id The id of the visit to be retrieved.
     * @return A Visit object
     */
    public Visit getById(Long id) {
        return visitRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Visit not found"));
    }

    /**
     * > The function creates a new visit, and saves it to the database
     *
     * @param date The date of the visit
     * @param medicalInterview The medical interview that the doctor has with the patient.
     * @param diagnosis The diagnosis of the patient.
     * @param patientId The id of the patient that the visit is for.
     * @param doctorId The id of the doctor who will be assigned to the visit.
     * @param medicalReceiptId The id of the medical receipt that will be associated with the visit.
     * @return Visit
     */
    public Visit create(LocalDate date, String medicalInterview, String diagnosis, Long patientId, Long doctorId,  Long medicalReceiptId) {
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        Doctor doctor = doctorRepository.findById(doctorId).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
        MedicalReceipt medicalReceipt = medicalReceiptRepository.findById(medicalReceiptId).orElseThrow(() -> new ResourceNotFoundException("MedicalReceipt not found"));
        Visit visit = Visit.builder()
                .date(date)
                .medicalInterview(medicalInterview)
                .diagnosis(diagnosis)
                .patient(patient)
                .doctor(doctor)
                .medicalPrescription(medicalReceipt)
                .build();
        return visitRepository.save(visit);
    }

    /**
     * It updates the visit with the given id.
     *
     * @param id The id of the visit to be updated.
     * @param date The date of the visit
     * @param medicalInterview The medical interview of the visit.
     * @param diagnosis The diagnosis of the visit.
     * @param patientId The ID of the patient that the visit is for.
     * @param doctorId The id of the doctor that will be assigned to the visit.
     * @param medicalPrescriptionId The id of the medical prescription that the visit is associated with.
     * @return Visit
     */
    public Visit update(Long id, LocalDate date, String medicalInterview, String diagnosis, Long patientId, Long doctorId, Long medicalPrescriptionId) {
        Visit visit = visitRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Visit not found"));
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        Doctor doctor = doctorRepository.findById(doctorId).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
        MedicalReceipt medicalReceipt = medicalReceiptRepository.findById(medicalPrescriptionId).orElseThrow(() -> new ResourceNotFoundException("MedicalReceipt not found"));
        visit.setDate(date);
        visit.setMedicalInterview(medicalInterview);
        visit.setDiagnosis(diagnosis);
        visit.setPatient(patient);
        visit.setDoctor(doctor);
        visit.setMedicalPrescription(medicalReceipt);
        return visitRepository.save(visit);
    }

    /**
     * Delete the visit with the given id.
     *
     * @param id The id of the visit to delete.
     */
    public void delete(Long id) {
        visitRepository.deleteById(id);
    }

    /**
     * Get all visits that happened today.
     *
     * @return A list of visits that are scheduled for today.
     */
    public List<Visit> getVisitsToday() {
        return visitRepository.findByDate(LocalDate.now());
    }
}
