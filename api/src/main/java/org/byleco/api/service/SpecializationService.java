package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.MedicalDepartment;
import org.byleco.api.model.Specialization;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.byleco.api.repository.SpecializationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpecializationService {
    private final SpecializationRepository specializationRepository;

    /**
     * Get all the specializations from the database and return them.
     *
     * @return A list of all the specializations in the database.
     */
    public List<Specialization> getSpecializations() {
        return specializationRepository.findAll();
    }

    /**
     * If the specialization with the given id exists, return it, otherwise throw a ResourceNotFoundException.
     *
     * @param id The id of the specialization to be retrieved.
     * @return Specialization
     */
    public Specialization getById(Long id) {
        return specializationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Specialization not found"));
    }

    /**
     * > Find all specializations by name
     *
     * @param name The name of the specialization.
     * @return A list of Specialization objects.
     */
    public List<Specialization> findAllByName(List<String> name) {
        return name.stream()
                .map(specName -> specializationRepository.findByName(specName)
                        .orElseThrow(() -> new ResourceNotFoundException("Specialization " + specName + " not found!")))
                .collect(Collectors.toList());
    }
}
