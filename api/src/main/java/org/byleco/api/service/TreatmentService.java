package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.*;
import org.byleco.api.repository.*;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TreatmentService {
    private final TreatmentRepository treatmentRepository;
    private final PatientRepository patientRepository;
    private final ExaminationRepository examinationRepository;
    private final VitalSignRepository vitalSignRepository;
    private final DoctorRepository doctorRepository;

    /**
     * Get all the treatments from the database and return them.
     *
     * @return A list of all treatments
     */
    public List<Treatment> getTreatments() {
        return treatmentRepository.findAll();
    }

    /**
     * If the treatment exists, return it, otherwise throw an exception.
     *
     * @param id The id of the treatment to be retrieved.
     * @return A Treatment object
     */
    public Treatment getById(Long id) {
        return treatmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Treatment not found"));
    }

    /**
     * > It creates a new treatment for a patient, with a doctor, examinations, vital signs and a room number
     *
     * @param date the date of the treatment
     * @param medicalInterview The medical interview of the patient.
     * @param diagnosis The diagnosis of the patient.
     * @param patientId the id of the patient
     * @param doctorId The id of the doctor who is creating the treatment.
     * @param examinationIds a list of examination ids
     * @param vitalSignIds a list of vital signs ids
     * @param numberOfRoom the number of the room where the patient is located
     * @return Treatment
     */
    public Treatment create(LocalDate date,
                            String medicalInterview,
                            String diagnosis,
                            Long patientId,
                            Long doctorId,
                            List<Long> examinationIds,
                            List<Long> vitalSignIds,
                            Integer numberOfRoom)
    {
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        Doctor doctor = doctorRepository.findById(doctorId).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
        List<Examination> examinations = examinationRepository.findAllById(examinationIds);
        List<VitalSign> vitalSigns = vitalSignRepository.findAllById(vitalSignIds);
        Treatment treatment = Treatment.builder()
                .date(date)
                .medicalInterview(medicalInterview)
                .diagnosis(diagnosis)
                .patient(patient)
                .doctor(doctor)
                .examinations(examinations)
                .vitalSigns(vitalSigns)
                .numberOfRoom(numberOfRoom)
                .build();
        return treatmentRepository.save(treatment);
    }

    /**
     * It updates the treatment with the given id.
     *
     * @param id The id of the treatment you want to update.
     * @param date The date of the treatment
     * @param medicalInterview String
     * @param diagnosis String
     * @param patientId The id of the patient that the treatment is for.
     * @param doctorId The id of the doctor who is treating the patient.
     * @param examinationsId List of examination ids
     * @param vitalSignsId List of vital signs ids
     * @param numberOfRoom The number of the room where the patient is located.
     * @return Treatment
     */
    public Treatment update(Long id, LocalDate date, String medicalInterview, String diagnosis, Long patientId, Long doctorId, List<Long> examinationsId, List<Long> vitalSignsId, Integer numberOfRoom) {
        Treatment treatment = treatmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Treatment not found"));
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        Doctor doctor = doctorRepository.findById(doctorId).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
        List<Examination> examinations = examinationRepository.findAllById(examinationsId);
        List<VitalSign> vitalSigns = vitalSignRepository.findAllById(vitalSignsId);
        treatment.setDate(date);
        treatment.setMedicalInterview(medicalInterview);
        treatment.setDiagnosis(diagnosis);
        treatment.setPatient(patient);
        treatment.setDoctor(doctor);
        treatment.setExaminations(examinations);
        treatment.setVitalSigns(vitalSigns);
        treatment.setNumberOfRoom(numberOfRoom);
        return treatmentRepository.save(treatment);
    }
}
