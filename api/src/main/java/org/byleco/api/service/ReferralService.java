package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.Patient;
import org.byleco.api.model.Referral;
import org.byleco.api.model.TypeOfReferral;
import org.byleco.api.repository.DoctorRepository;
import org.byleco.api.repository.PatientRepository;
import org.byleco.api.repository.ReferralRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReferralService {
    private final ReferralRepository referralRepository;
    private final DoctorRepository doctorRepository;
    private final PatientRepository patientRepository;

    /**
     * Get all the referrals from the database and return them as a list.
     *
     * @return A list of all referrals in the database.
     */
    public List<Referral> getReferrals() {
        return referralRepository.findAll();
    }

    /**
     * If the referral with the given id exists, return it, otherwise throw a ResourceNotFoundException.
     *
     * @param id The id of the referral to be retrieved.
     * @return A referral object
     */
    public Referral getById(Long id) {
        return referralRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Referral not found"));
    }

    /**
     * > Create a referral with the given status, doctor, patient, and description
     *
     * @param status TypeOfReferral
     * @param doctorId The id of the doctor who is referring the patient.
     * @param patientId The id of the patient that the referral is for.
     * @param description The description of the referral
     * @return A referral object
     */
    public Referral create(TypeOfReferral status, Long doctorId, Long patientId, String description) {
        Doctor doctor = doctorRepository.findById(doctorId).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        Referral referral = Referral.builder()
                .status(status)
                .doctor(doctor)
                .patient(patient)
                .description(description)
                .isChecked(false)
                .build();
        return referralRepository.save(referral);
    }

    /**
     * It takes in a referral id and a boolean value, and then it sets the referral's isChecked value to the boolean value
     *
     * @param id The id of the referral you want to check.
     * @param check true or false
     * @return A referral object.
     */
    public Referral checkReferral(Long id, Boolean check) {
        Referral referral = referralRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Referral not found"));
        referral.setIsChecked(check);
        return referralRepository.save(referral);
    }

    /**
     * Update a referral by id, status, doctor id, patient id, and description.
     *
     * @param id The id of the referral to be updated
     * @param status The status of the referral.
     * @param doctorId The id of the doctor who will be assigned to the referral.
     * @param patientId The id of the patient that the referral is for.
     * @param description The description of the referral
     * @return A referral object.
     */
    public Referral update(Long id, TypeOfReferral status, Long doctorId, Long patientId, String description) {
        Referral referral = referralRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Referral not found"));
        Doctor doctor = doctorRepository.findById(doctorId).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        referral.setStatus(status);
        referral.setDoctor(doctor);
        referral.setPatient(patient);
        referral.setDescription(description);
        return referralRepository.save(referral);
    }
}
