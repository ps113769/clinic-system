package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.Drug;
import org.byleco.api.model.MedicalReceipt;
import org.byleco.api.model.Patient;
import org.byleco.api.repository.DrugRepository;
import org.byleco.api.repository.MedicalReceiptRepository;
import org.byleco.api.repository.PatientRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicalReceiptService {
    private final MedicalReceiptRepository medicalReceiptRepository;
    private final DrugRepository drugRepository;
    private final PatientRepository patientRepository;

    /**
     * Get all the medical receipts from the database and return them.
     *
     * @return A list of all medical receipts
     */
    public List<MedicalReceipt> getMedicalReceipts() {
        return medicalReceiptRepository.findAll();
    }

    /**
     * If the receipt exists, return it, otherwise throw an exception.
     *
     * @param id The id of the receipt to be fetched.
     * @return A MedicalReceipt object
     */
    public MedicalReceipt fetch(Long id) {
        return medicalReceiptRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Receipt not found"));
    }

    /**
     * > Create a new medical receipt for a patient with a list of drugs and an expiration date
     *
     * @param patientId The id of the patient
     * @param drugIds a list of drug ids
     * @param expirationDate The date when the receipt expires.
     * @return MedicalReceipt
     */
    public MedicalReceipt create(Long patientId, List<Long> drugIds, LocalDate expirationDate) {
        Patient patient = patientRepository.findById(patientId).orElseThrow(()-> new ResourceNotFoundException("Patient not found"));
        List<Drug> drugs = drugRepository.findAllById(drugIds);
        MedicalReceipt medicalReceipt = MedicalReceipt.builder()
                .patient(patient)
                .drugs(drugs)
                .expirationDate(expirationDate)
                .build();
        return medicalReceiptRepository.save(medicalReceipt);
    }

    /**
     * > Update a medical receipt by id, with a patient id, a list of drug ids, and an expiration date
     *
     * @param id the id of the receipt to be updated
     * @param patientId The id of the patient that the receipt belongs to.
     * @param drugIds a list of drug ids
     * @param expirationDate The date when the receipt expires.
     * @return A MedicalReceipt object
     */
    public MedicalReceipt update(Long id, Long patientId, List<Long> drugIds, LocalDate expirationDate) {
        MedicalReceipt medicalReceipt = medicalReceiptRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Receipt not found"));
        Patient patient = patientRepository.findById(patientId).orElseThrow(()-> new ResourceNotFoundException("Patient not found"));
        List<Drug> drugs = drugRepository.findAllById(drugIds);
        medicalReceipt.setPatient(patient);
        medicalReceipt.setDrugs(drugs);
        medicalReceipt.setExpirationDate(expirationDate);
        return medicalReceiptRepository.save(medicalReceipt);
    }

    /**
     * Fetch all medical receipts for a given patient.
     *
     * @param patientId The id of the patient
     * @return A list of MedicalReceipts
     */
    public List<MedicalReceipt> fetchByPatientId(Long patientId) {
        return medicalReceiptRepository.findByPatientId(patientId);
    }
}
