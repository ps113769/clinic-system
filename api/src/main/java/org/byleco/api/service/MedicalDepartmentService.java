package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.MedicalDepartment;
import org.byleco.api.repository.DoctorRepository;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicalDepartmentService {

    private final MedicalDepartmentRepository medicalDepartmentRepository;

    /**
     * This function returns a list of all medical departments.
     *
     * @return A list of all medical departments.
     */
    public List<MedicalDepartment> getMedicalDepartaments() {
        return medicalDepartmentRepository.findAll();
    }

    /**
     * If the department exists, return it, otherwise throw an exception.
     *
     * @param id The id of the department to be retrieved.
     * @return A MedicalDepartment object
     */
    public MedicalDepartment getById(Long id) {
        return medicalDepartmentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Department not found"));
    }

}
