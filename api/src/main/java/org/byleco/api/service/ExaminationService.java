package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Examination;
import org.byleco.api.model.Patient;
import org.byleco.api.payload.exam.ExamRequest;
import org.byleco.api.repository.ExaminationRepository;
import org.byleco.api.repository.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ExaminationService {
    private final ExaminationRepository examRepository;
    private final PatientRepository patientRepository;

    /**
     * Get all examinations from the database and return them as a list.
     *
     * @return A list of all examinations in the database.
     */
    public List<Examination> getExams() {
        return examRepository.findAll();
    }

    /**
     * We create a new exam object using the builder pattern, and then save it to the database
     *
     * @param exam The exam object that is being created.
     * @return A new exam object is being returned.
     */
    public Examination createExam(ExamRequest exam) {
        Patient patient = patientRepository.findById(exam.getPatient_id()).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));

        Examination examination = Examination.builder()
                .name(exam.getName())
                .description(exam.getDescription())
                .date(exam.getDate())
                .patient(patient)
                .result(exam.getResult())
                .realized(exam.getRealized())
                .build();
        return examRepository.save(examination);
    }

    /**
     * Delete an exam by id.
     *
     * @param id The id of the exam to be deleted.
     */
    public void deleteExam(Long id) {
        examRepository.deleteById(id);
    }

    /**
     * If the exam exists, return it, otherwise throw an exception.
     *
     * @param examId The id of the exam to be retrieved.
     * @return Examination object
     */
    public Examination getExam(Long examId) {
        return examRepository.findById(examId).orElseThrow(() -> new ResourceNotFoundException("Exam not found"));
    }

    /**
     * It updates an exam with the given id, with the given request, and returns the updated exam
     *
     * @param examId The id of the exam you want to update.
     * @param request The request body of the request.
     * @return Examination
     */
    public Examination updateExam(Long examId, ExamRequest request) {
        Patient patient = patientRepository.findById(request.getPatient_id()).orElseThrow(() -> new ResourceNotFoundException("Patient not found"));
        Examination exam = examRepository.findById(examId).orElseThrow(() -> new ResourceNotFoundException("Exam not found"));
        exam.setName(request.getName());
        exam.setPatient(patient);
        exam.setDate(request.getDate());
        exam.setDescription(request.getDescription());
        exam.setResult(request.getResult());
        exam.setRealized(request.getRealized());
        return examRepository.save(exam);
    }
}
