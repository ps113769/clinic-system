package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Role;
import org.byleco.api.model.RoleName;
import org.byleco.api.repository.RoleRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    /**
     * Find a role by name, or throw an exception if it doesn't exist.
     *
     * @param roleName The name of the role you want to find.
     * @return A role object
     */
    public Role findByName(RoleName roleName) {
        return roleRepository.findByName(roleName).orElseThrow(() -> new ResourceNotFoundException("Role not found!"));
    }

    /**
     * If the role ROLE_ADMIN doesn't exist, create it
     */
    @PostConstruct
    public void createDefaultRoles() {
        roleRepository.findByName(RoleName.ROLE_ADMIN).orElseGet(() -> {
            Role userRole = new Role(1L, RoleName.ROLE_ADMIN);
            return roleRepository.save(userRole);
        });
    }
}
