package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.*;
import org.byleco.api.payload.doctor.DoctorRequest;
import org.byleco.api.payload.nurse.NurseSelectListResponse;
import org.byleco.api.payload.doctor.DoctorSelectListResponse;
import org.byleco.api.repository.DoctorRepository;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.byleco.api.repository.SpecializationRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DoctorService {
    private final DoctorRepository doctorRepository;
    private final MedicalDepartmentRepository medicalDepartmentRepository;
    private final SpecializationRepository specializationRepository;

    /**
     * Get all doctors from the database and return them as a list.
     *
     * @return A list of all doctors in the database.
     */
    public List<Doctor> getDoctors() {
        return doctorRepository.findAll();
    }

    public List<DoctorSelectListResponse> getDoctorsSelectList() {
        List<Doctor> doctorList = doctorRepository.findAll();

        List<DoctorSelectListResponse> doctorSelectListResponseList = new ArrayList<>();

        for (Doctor doctor: doctorList
        ) {
            DoctorSelectListResponse item = new DoctorSelectListResponse();
            item.setId(doctor.getId());
            item.setFullName(doctor.getFirstName() + " " + doctor.getLastName());
            doctorSelectListResponseList.add(item);
        }
        return doctorSelectListResponseList;
    }
    public Doctor fetchDoctor(Long id) {
        return doctorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
    }

    /**
     * We're creating a new Doctor object, setting its properties, and saving it to the database
     *
     * @param request the request object that contains the data that the user has sent to the server.
     * @return Doctor
     */
    public Doctor createDoctor(DoctorRequest request) {
        MedicalDepartment medicalDepartment = medicalDepartmentRepository.findById(request.getMedicalDepartment_id()).orElseThrow(() -> new ResourceNotFoundException("Medical Department not found"));

        List<Specialization> specializations = new ArrayList<>();

        for (Long id : request.getSpecializations()) {
            specializations.add(specializationRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Specialization not found")));
        }

        Doctor doctor = Doctor.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .medicalDepartment(medicalDepartment)
                .specializations(specializations)
                .build();

        return doctorRepository.save(doctor);
    }

    /**
     * We're updating a doctor by id, and we're using the request body to update the doctor's first name, last name,
     * specializations, and medical department
     *
     * @param id      The id of the doctor to be updated
     * @param request The request body of the request.
     * @return Doctor
     */
    public Doctor updateDoctor(Long id, DoctorRequest request) {
        Doctor doctor = doctorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));

        MedicalDepartment medicalDepartment = medicalDepartmentRepository.findById(request.getMedicalDepartment_id()).orElseThrow(() -> new ResourceNotFoundException("Medical Department not found"));

        List<Specialization> specializations = new ArrayList<>();

        for (Long specializationId : request.getSpecializations()
        ) {
            specializations.add(specializationRepository.findById(specializationId).orElseThrow(() -> new ResourceNotFoundException("Specialization not found")));
        }

        doctor.setFirstName(request.getFirstName());
        doctor.setLastName(request.getLastName());
        doctor.setSpecializations(specializations);
        doctor.setMedicalDepartment(medicalDepartment);

        return doctorRepository.save(doctor);
    }

    /**
     * Delete a doctor by id.
     *
     * @param id The id of the doctor to be deleted.
     */
    public void deleteDoctor(Long id) {
        doctorRepository.deleteById(id);
    }
}
