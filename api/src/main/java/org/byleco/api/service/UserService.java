package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Role;
import org.byleco.api.model.RoleName;
import org.byleco.api.model.User;
import org.byleco.api.repository.UserRepository;
import org.byleco.api.security.UserPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    /**
     * Get the user from the database using the user principal's id.
     *
     * @param userPrincipal The UserPrincipal object that is passed to the method.
     * @return A user object
     */
    public User getUserFromPrincipal(UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId()).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
    }

    /**
     * Save the user to the database.
     *
     * @param user The user object that is to be saved.
     * @return The user object is being returned.
     */
    public User save(User user) {
        return userRepository.save(user);
    }

    /**
     * Find a user by their username, or throw an exception if they don't exist.
     *
     * @param username The username of the user you want to find.
     * @return A user object
     */
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
    }

    /**
     * If the user is not found, throw a ResourceNotFoundException.
     *
     * @param id The id of the user to be retrieved.
     * @return A user object
     */
    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
    }

    @PostConstruct
    private void createDummyUser() {
        Role role = roleService.findByName(RoleName.ROLE_ADMIN);
        User user = User.builder()
                .firstName("Jan")
                .lastName("Kowalski")
                .username("admin123")
                .password(passwordEncoder.encode("admin123"))
                .roles(Collections.singleton(role))
                .build();
        userRepository.save(user);
    }
}
