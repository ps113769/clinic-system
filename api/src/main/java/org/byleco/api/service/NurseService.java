package org.byleco.api.service;

import lombok.RequiredArgsConstructor;
import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.*;
import org.byleco.api.payload.doctor.DoctorRequest;
import org.byleco.api.payload.nurse.NurseRequest;
import org.byleco.api.payload.nurse.NurseSelectListResponse;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.byleco.api.repository.NurseRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NurseService {
    private final NurseRepository nurseRepository;
    private final MedicalDepartmentRepository medicalDepartmentRepository;

    /**
     * Get all the nurses from the database and return them as a list.
     *
     * @return A list of all nurses in the database.
     */
    public List<Nurse> getNurses() {
        return nurseRepository.findAll();
    }

    /**
     * It gets all the nurses from the database, creates a list of NurseSelectListResponse objects, and populates the list
     * with the id and full name of each nurse
     *
     * @return A list of NurseSelectListResponse objects.
     */
    public List<NurseSelectListResponse> getNursesSelectList() {
        List<Nurse> nurseList = nurseRepository.findAll();

        List<NurseSelectListResponse> nurseSelectListResponseList = new ArrayList<>();

        for (Nurse nurse: nurseList) {
            NurseSelectListResponse item = new NurseSelectListResponse();
            item.setId(nurse.getId());
            item.setFullName(nurse.getFirstName() + " " + nurse.getLastName());
            nurseSelectListResponseList.add(item);
        }
        return nurseSelectListResponseList;
    }

    /**
     * If the nurse exists, return it, otherwise throw an exception.
     *
     * @param id The id of the nurse to be fetched.
     * @return A Nurse object
     */
    public Nurse fetchNurse(Long id) {
        return nurseRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Nurse not found"));
    }

    /**
     * > Create a new Nurse object and save it to the database
     *
     * @param request The request object that contains the data that the user has sent to the server.
     * @return A Nurse object
     */
    public Nurse createNurse(NurseRequest request) {
        MedicalDepartment medicalDepartment = medicalDepartmentRepository.findById(request.getMedicalDepartment_id()).orElseThrow(() -> new ResourceNotFoundException("Medical Department not found"));

        Nurse nurse = Nurse.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .medicalDepartment(medicalDepartment)
                .build();

        return nurseRepository.save(nurse);
    }

    /**
     * Update a nurse with the given id, using the given request, and return the updated nurse.
     *
     * @param id The id of the nurse you want to update.
     * @param request The request body that contains the data that will be used to update the nurse.
     * @return Nurse
     */
    public Nurse updateNurse(Long id, NurseRequest request) {
        Nurse nurse = nurseRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Nurse not found"));

        MedicalDepartment medicalDepartment = medicalDepartmentRepository.findById(request.getMedicalDepartment_id()).orElseThrow(() -> new ResourceNotFoundException("Medical Department not found"));

        nurse.setFirstName(request.getFirstName());
        nurse.setLastName(request.getLastName());
        nurse.setMedicalDepartment(medicalDepartment);

        return nurseRepository.save(nurse);
    }

    /**
     * Delete a nurse by id.
     *
     * @param id The id of the nurse to be deleted.
     */
    public void deleteNurse(Long id) {
        nurseRepository.deleteById(id);
    }

}
