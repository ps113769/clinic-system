//package org.byleco.api.config;
//
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.util.List;
//import java.util.Scanner;
//
//@Configuration
//public class DatabaseConfig {
//    private String url;
//    private String dbName;
//    private String username;
//    private String password;
//
//    @Bean
//    public DataSource getDataSource() {
//        readDataFromConfigFile();
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//        dataSourceBuilder.driverClassName("com.mysql.cj.jdbc.Driver");
//        dataSourceBuilder.url(url + "/" + dbName);
//        dataSourceBuilder.username(username);
//        dataSourceBuilder.password(password);
//        return dataSourceBuilder.build();
//    }
//
//    private void readDataFromConfigFile() {
//        try {
//            File myObj = new File("db.config");
//            Scanner myReader = new Scanner(myObj);
//            while (myReader.hasNextLine()) {
//                String data = myReader.nextLine();
//                if (data.startsWith("db.user")) username = data.substring(data.indexOf("=") + 1);
//                else if (data.startsWith("db.password")) password = data.substring(data.indexOf("=") + 1);
//                else if (data.startsWith("db.name")) dbName = data.substring(data.indexOf("=") + 1);
//                else if (data.startsWith("db.address")) url = data.substring(data.indexOf("=") + 1);
//                System.out.println(data);
//            }
//            myReader.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        System.out.println(url + "/" + dbName);
//        System.out.println(username);
//        System.out.println(password);
//    }
//}
