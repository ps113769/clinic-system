package org.byleco.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class MedicalOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty(message = "Nazwa nie może być pusta")
    private String name;
    @NotEmpty(message = "Opis nie może być pusty")
    private String description;

    @NotNull(message = "Pacjent nie moze być pusty")
    @OneToOne
    private Patient patient;

    @NotEmpty(message = "Lista pielęgniarek nie moze być pusta")
    @OneToMany
    private List<Nurse> nurse;
}
