package org.byleco.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Treatment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate date;
    private String medicalInterview;
    private String diagnosis;

    @OneToMany
    private List<Examination> examinations;

    @OneToOne
    private Patient patient;

    @OneToOne
    private Doctor doctor;

    @OneToMany
    private List<VitalSign> vitalSigns;

    private Integer numberOfRoom;
}
