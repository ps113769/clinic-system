package org.byleco.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Nurse extends User {

    @ManyToOne
    private MedicalDepartment medicalDepartment;

    @OneToMany
    private List<MedicalOrder> medicalOrders;
}
