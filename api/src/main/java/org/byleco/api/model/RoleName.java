package org.byleco.api.model;

public enum  RoleName {
    ROLE_DOCTOR,
    ROLE_NURSE,
    ROLE_LAB_TECHNICIAN,
    ROLE_RECEPCIONIST,
    ROLE_ADMIN
}
