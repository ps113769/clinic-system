package org.byleco.api.model;

public enum TypeOfReferral {
    SPECIALIST_CLINIC,
    MEDICAL_EXAMINATION
}
