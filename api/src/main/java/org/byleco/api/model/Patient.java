package org.byleco.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Name cannot be empty")
    private String name;
    @NotNull(message = "surname cannot be empty")
    private String surname;
    @NotNull(message = "phone cannot be empty")
    private String phone;
    @NotNull(message = "address cannot be empty")
    private String address;
    @NotNull(message = "city cannot be empty")
    private String city;
    @NotNull(message = "Nastateme cannot be empty")
    private String state;
    @NotNull(message = "zip cannot be empty")
    private String zip;
    @NotNull(message = "country cannot be empty")
    private String country;
}
