package org.byleco.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Referral {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private TypeOfReferral status;

    @ManyToOne
    private Doctor doctor;

    @ManyToOne
    private Patient patient;

    private String description;

    private Boolean isChecked;

    @PrePersist
    void createdAt() {
        this.createdAt = LocalDateTime.now();
    }
}
