package org.byleco.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Doctor extends User {

    @OneToMany
    private List<Visit> visits;

    @ManyToMany
    private List<Specialization> specializations;

    @OneToMany
    private List<Referral> referrals;

    @OneToMany
    private List<Treatment> treatments;

    @ManyToOne
    private MedicalDepartment medicalDepartment;
}
