package org.byleco.api.payload.doctor;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DoctorSelectListResponse {
    private Long id;
    private String fullName;
}
