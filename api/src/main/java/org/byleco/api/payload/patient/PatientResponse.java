package org.byleco.api.payload.patient;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PatientResponse {
    private Long id;
    private String name;
    private String surname;
    private String phone;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String country;
    private String fullName;
}
