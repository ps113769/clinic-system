package org.byleco.api.payload.visit;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class CreateVisitRequest {
    private String medicalInterview;
    private String diagnosis;
    private LocalDate date;
    private Long patientId;
    private Long doctorId;
    private Long medicalPrescriptionId;
}
