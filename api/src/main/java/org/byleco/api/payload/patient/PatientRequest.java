package org.byleco.api.payload.patient;

import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
public class PatientRequest {
    @NotNull(message = "Name cannot be empty")
    @NotEmpty(message = "Name cannot be empty")
    private String name;
    @NotNull(message = "surname cannot be empty")
    @NotEmpty(message = "surname cannot be empty")
    private String surname;
    @NotNull(message = "phone cannot be empty")
    @NotEmpty(message = "phone cannot be empty")
    private String phone;
    @NotNull(message = "address cannot be empty")
    @NotEmpty(message = "address cannot be empty")
    private String address;
    @NotNull(message = "city cannot be empty")
    @NotEmpty(message = "city cannot be empty")
    private String city;
    @NotNull(message = "state cannot be empty")
    @NotEmpty(message = "state cannot be empty")
    private String state;
    @NotNull(message = "zip cannot be empty")
    @NotEmpty(message = "zip cannot be empty")
    private String zip;
    @NotNull(message = "country cannot be empty")
    @NotEmpty(message = "country cannot be empty")
    private String country;
}
