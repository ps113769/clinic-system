package org.byleco.api.payload.specialization;

import lombok.Getter;

import java.util.List;

@Getter
public class SpecializationsByNameRequest {
    private List<String> specializationNames;
}
