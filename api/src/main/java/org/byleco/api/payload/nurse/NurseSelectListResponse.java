package org.byleco.api.payload.nurse;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
@NoArgsConstructor
public class NurseSelectListResponse {
    private Long id;
    private String fullName;
}
