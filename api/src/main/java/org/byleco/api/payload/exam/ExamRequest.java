package org.byleco.api.payload.exam;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Builder
public class ExamRequest {
    @NotNull(message = "Name cannot be empty")
    @NotEmpty(message = "Name cannot be empty")
    private String name;
    @NotNull(message = "description cannot be empty")
    @NotEmpty(message = "description cannot be empty")
    private String description;
    private String result;
    private long patient_id;
    private LocalDate date;
    private Boolean realized;

}