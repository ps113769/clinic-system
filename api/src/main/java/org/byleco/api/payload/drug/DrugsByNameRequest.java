package org.byleco.api.payload.drug;

import lombok.Getter;

import java.util.List;

@Getter
public class DrugsByNameRequest {
    private List<String> drugNames;
}
