package org.byleco.api.payload.medicalOrder;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.byleco.api.model.Nurse;
import org.byleco.api.model.Patient;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@SuperBuilder
public class MedicalOrderRequest {
    @NotNull(message = "Name cannot be null")
    @NotEmpty(message = "Name cannot be empty")
    private String name;
    @NotNull(message = "Description cannot be null")
    @NotEmpty(message = "Description cannot be empty")
    private String description;

    @NotNull(message = "Patient cannot be null")
    private long patient_id;

    private List<Long> nurses;
}
