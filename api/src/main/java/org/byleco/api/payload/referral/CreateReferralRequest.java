package org.byleco.api.payload.referral;

import lombok.Getter;
import org.byleco.api.model.TypeOfReferral;

@Getter
public class CreateReferralRequest {
    private TypeOfReferral status;
    private Long doctorId;
    private Long patientId;
    private String description;
}
