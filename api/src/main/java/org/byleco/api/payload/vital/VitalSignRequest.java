package org.byleco.api.payload.vital;

import lombok.Getter;
import org.byleco.api.model.User;

import javax.validation.constraints.NotNull;

@Getter
public class VitalSignRequest {
    @NotNull(message = "Name cannot be empty")
    private String name;
    @NotNull(message = "Description cannot be empty")
    private String description;

    @NotNull(message = "Patient cannot be null")
    private Long patient_id;

    @NotNull(message = "Medical user cannot be null")
    private Long medicalUser_id;
}
