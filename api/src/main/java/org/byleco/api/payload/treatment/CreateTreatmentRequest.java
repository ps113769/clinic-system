package org.byleco.api.payload.treatment;

import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@Getter
public class CreateTreatmentRequest {
    private LocalDate date;
    private String medicalInterview;
    private String diagnosis;
    private List<Long> examinationsId;
    private Long patientId;
    private Long doctorId;
    private List<Long> vitalSignsId;
    private Integer numberOfRoom;
}
