package org.byleco.api.payload.medicalReceipt;

import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@Getter
public class CreateMedicalReceiptRequest {
    private Long patientId;
    private List<Long> drugIds;
    private LocalDate expirationDate;
}
