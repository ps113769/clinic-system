package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.Nurse;
import org.byleco.api.payload.doctor.DoctorRequest;
import org.byleco.api.payload.nurse.NurseRequest;
import org.byleco.api.payload.nurse.NurseSelectListResponse;
import org.byleco.api.service.NurseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class NurseController {
    private final NurseService nurseService;

    /**
     * The function is a GET request that returns a list of nurses
     *
     * @return A list of nurses
     */
    @GetMapping("/nurses")
    public ResponseEntity<List<Nurse>> getNurses() {
        List<Nurse> nurses = nurseService.getNurses();
        return ResponseEntity.ok(nurses);
    }

    /**
     * It returns a list of nurses in a format that is suitable for a select list
     *
     * @return A list of nurses
     */
    @GetMapping("/nurses/select")
    public ResponseEntity<List<NurseSelectListResponse>> getNursesSelect() {
        List<NurseSelectListResponse> nurses = nurseService.getNursesSelectList();
        return ResponseEntity.ok(nurses);
    }

    /**
     * This function fetches a nurse by id
     *
     * @param id The id of the nurse to be fetched.
     * @return A nurse object
     */
    @GetMapping("/nurse/{id}")
    public ResponseEntity<Nurse> fetchNurse(@PathVariable Long id) {
        Nurse nurse = nurseService.fetchNurse(id);

        return ResponseEntity.ok(nurse);
    }

    /**
     * This function takes in a NurseRequest object, validates it, and then creates a new Nurse object
     *
     * @param nurse The nurse object that will be created.
     * @return A ResponseEntity object is being returned.
     */
    @PostMapping("/nurse")
    public ResponseEntity<Nurse> createNurse(@RequestBody @Valid NurseRequest nurse) {
        Nurse newNurse = nurseService.createNurse(nurse);
        return ResponseEntity.ok(newNurse);
    }

    /**
     * This function updates a nurse's information in the database
     *
     * @param id The id of the nurse to be updated.
     * @param nurse The nurse object that will be updated.
     * @return ResponseEntity<Nurse>
     */
    @PutMapping("/nurse/{id}")
    public ResponseEntity<Nurse> updateNurse(@PathVariable Long id, @RequestBody @Valid NurseRequest nurse) {
        Nurse updatedNurse = nurseService.updateNurse(id, nurse);
        return ResponseEntity.ok(updatedNurse);
    }
}
