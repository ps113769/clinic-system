package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Drug;
import org.byleco.api.payload.drug.DrugsByNameRequest;
import org.byleco.api.service.DrugService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class DrugController {
    private final DrugService drugService;

    @GetMapping("/drugs")
    public ResponseEntity<List<Drug>> getDrugs() {
        List<Drug> drugs = drugService.getDrugs();
        return ResponseEntity.ok(drugs);
    }
    @GetMapping("/drug/{id}")
    public ResponseEntity<Drug> getDrugByName(@PathVariable Long id) {
        Drug drug = drugService.getById(id);
        return ResponseEntity.ok(drug);
    }

    @GetMapping("/drugs/by-name")
    public ResponseEntity<List<Drug>> getDrugsByName(@RequestBody DrugsByNameRequest body) {
        List<Drug> drugs = drugService.findAllByName(body.getDrugNames());
        return ResponseEntity.ok(drugs);
    }
}
