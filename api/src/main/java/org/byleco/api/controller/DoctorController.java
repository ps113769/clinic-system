package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.MedicalReceipt;
import org.byleco.api.payload.doctor.DoctorRequest;
import org.byleco.api.payload.doctor.DoctorSelectListResponse;
import org.byleco.api.service.DoctorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class DoctorController {
    private final DoctorService doctorService;

    /**
     * It returns a list of doctors
     *
     * @return A list of doctors
     */
    @GetMapping("/doctors")
    public ResponseEntity<List<Doctor>> getDoctors() {
        List<Doctor> patients = doctorService.getDoctors();
        return ResponseEntity.ok(patients);
    }

    /**
     * It fetches a doctor by id and returns a response entity with the doctor
     *
     * @param doctorId The id of the doctor to fetch.
     * @return A ResponseEntity object is being returned.
     */
    @GetMapping("/doctor/{doctorId}")
    public ResponseEntity<Doctor> fetchDoctor(@PathVariable Long doctorId) {
        Doctor doctor = doctorService.fetchDoctor(doctorId);

        return ResponseEntity.ok(doctor);
    }

    /**
     * This function takes a DoctorRequest object, validates it, and then creates a new Doctor object
     *
     * @param doctor The object that will be created.
     * @return A ResponseEntity object is being returned.
     */
    @PostMapping("/doctor")
    public ResponseEntity<Doctor> createDoctor(@RequestBody @Valid DoctorRequest doctor) {
        Doctor newDoctor = doctorService.createDoctor(doctor);
        return ResponseEntity.ok(newDoctor);
    }
    @GetMapping("/doctors/select")
    public ResponseEntity<List<DoctorSelectListResponse>> getDoctorsSelect() {
        List<DoctorSelectListResponse> doctors = doctorService.getDoctorsSelectList();
        return ResponseEntity.ok(doctors);
    }

    /**
     * It takes a doctorId and a doctorRequest, and returns a doctor
     *
     * @param doctorId The id of the doctor to be updated.
     * @param doctor The doctor object that will be updated.
     * @return A ResponseEntity with the updated Doctor object.
     */
    @PutMapping("/doctor/{doctorId}")
    public ResponseEntity<Doctor> updateDoctor(@PathVariable Long doctorId, @RequestBody @Valid DoctorRequest doctor) {
        Doctor updatedDoctor = doctorService.updateDoctor(doctorId, doctor);
        return ResponseEntity.ok(updatedDoctor);
    }

    /**
     * It deletes a doctor from the database
     *
     * @param doctorId The id of the doctor to be deleted.
     * @return ResponseEntity.noContent().build();
     */
    @DeleteMapping("/doctor/{doctorId}")
    public ResponseEntity<Void> deleteDoctor(@PathVariable Long doctorId) {
        doctorService.deleteDoctor(doctorId);
        return ResponseEntity.noContent().build();
    }

}
