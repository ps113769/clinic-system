package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.MedicalDepartment;
import org.byleco.api.model.Specialization;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.byleco.api.service.MedicalDepartmentService;
import org.byleco.api.service.SpecializationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class MedicalDepartmentController {
    private final MedicalDepartmentService medicalDepartmentService;

    /**
     * It returns a list of medical departments
     *
     * @return List of MedicalDepartments
     */
    @GetMapping("/medical-departments")
    public ResponseEntity<List<MedicalDepartment>> getMedicalDepartments() {
        List<MedicalDepartment> medicalDepartments = medicalDepartmentService.getMedicalDepartaments();
        return ResponseEntity.ok(medicalDepartments);
    }
    /**
     * This function is used to get a medical department by its id
     *
     * @param id The id of the medical department you want to get.
     * @return ResponseEntity<MedicalDepartment>
     */
    @GetMapping("/medical-department/{id}")
    public ResponseEntity<MedicalDepartment> getMedicalDepartmentByName(@PathVariable Long id) {
        MedicalDepartment medicalDepartment = medicalDepartmentService.getById(id);
        return ResponseEntity.ok(medicalDepartment);
    }
}
