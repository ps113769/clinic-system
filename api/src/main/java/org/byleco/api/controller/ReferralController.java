package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Referral;
import org.byleco.api.payload.referral.CreateReferralRequest;
import org.byleco.api.service.ReferralService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ReferralController {
    private final ReferralService referralService;

    /**
     * This function returns a list of referrals
     *
     * @return A list of referrals
     */
    @GetMapping("/referrals")
    public ResponseEntity<List<Referral>> getReferrals() {
        List<Referral> referrals = referralService.getReferrals();
        return ResponseEntity.ok(referrals);
    }

    /**
     * This function is a GET request that takes in a referral id and returns a referral object
     *
     * @param id The id of the referral you want to get.
     * @return A referral object
     */
    @GetMapping("/referral/{id}")
    public ResponseEntity<Referral> getReferralById(@PathVariable Long id) {
        Referral referral = referralService.getById(id);
        return ResponseEntity.ok(referral);
    }

    /**
     * It takes a referral id and a boolean value, and returns a referral object
     *
     * @param id the id of the referral
     * @param check true or false
     * @return A referral object
     */
    @PutMapping("/referral/check/{id}")
    public ResponseEntity<Referral> checkReferral(@PathVariable Long id, @RequestParam Boolean check) {
        Referral referral = referralService.checkReferral(id, check);
        return ResponseEntity.ok(referral);
    }

    /**
     * It creates a new referral and returns it
     *
     * @param body The request body.
     * @return A new referral object
     */
    @PostMapping("/referral")
    public ResponseEntity<Referral> createReferral(@RequestBody CreateReferralRequest body) {
        Referral newReferral = referralService.create(body.getStatus(),
                body.getDoctorId(),
                body.getPatientId(),
                body.getDescription()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(newReferral);
    }

    /**
     * It takes a referral id, and a request body, and updates the referral with the given id with the values in the
     * request body
     *
     * @param id The id of the referral you want to update
     * @param body The request body, which is a JSON object.
     * @return A ResponseEntity object is being returned.
     */
    @PutMapping("/referral/{id}")
    public ResponseEntity<Referral> updateReferral(@PathVariable Long id, @RequestBody CreateReferralRequest body) {
        Referral updatedReferral = referralService.update(id, body.getStatus(),
                body.getDoctorId(),
                body.getPatientId(),
                body.getDescription()
        );
        return ResponseEntity.ok(updatedReferral);
    }
}
