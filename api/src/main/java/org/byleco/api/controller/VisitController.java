package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.Visit;
import org.byleco.api.model.MedicalReceipt;
import org.byleco.api.payload.visit.CreateVisitRequest;
import org.byleco.api.service.VisitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class  VisitController {
    private final VisitService visitService;

    /**
     * The function is a GET request that returns a list of visits
     *
     * @return A list of visits
     */
    @GetMapping("/visits")
    public ResponseEntity<List<Visit>> getVisits() {
        List<Visit> visits = visitService.getVisits();
        return ResponseEntity.ok(visits);
    }

    /**
     * Get all visits that happened today.
     *
     * @return A list of visits that are happening today.
     */
    @GetMapping("/visits/today")
    public ResponseEntity<List<Visit>> getVisitsToday() {
        List<Visit> visits = visitService.getVisitsToday();
        return ResponseEntity.ok(visits);
    }

    /**
     * > This function fetches a visit by id and returns it as a response entity
     *
     * @param id The id of the visit to fetch
     * @return A ResponseEntity object is being returned.
     */
    @GetMapping("/visit/{id}")
    public ResponseEntity<Visit> fetch(@PathVariable Long id) {
        Visit visit = visitService.getById(id);
        return ResponseEntity.ok(visit);
    }

    /**
     * It creates a new visit and returns it
     *
     * @param body The body of the request.
     * @return ResponseEntity<Visit>
     */
    @PostMapping("/visit")
    public ResponseEntity<Visit> create(@RequestBody CreateVisitRequest body) {
        Visit newVisit = visitService.create(
                body.getDate(),
                body.getMedicalInterview(),
                body.getDiagnosis(),
                body.getPatientId(),
                body.getDoctorId(),
                body.getMedicalPrescriptionId()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(newVisit);
    }

    /**
     * It takes the id of the visit, the date, the medical interview, the diagnosis, the patient id, the doctor id, and the
     * medical prescription id, and updates the visit with the given id with the given values
     *
     * @param id the id of the visit to update
     * @param body The request body, containing all the fields required to update a visit.
     * @return ResponseEntity<Visit>
     */
    @PutMapping("/visit/{id}")
    public ResponseEntity<Visit> update(@PathVariable Long id, @RequestBody CreateVisitRequest body) {
        Visit updatedVisit = visitService.update(
                id,
                body.getDate(),
                body.getMedicalInterview(),
                body.getDiagnosis(),
                body.getPatientId(),
                body.getDoctorId(),
                body.getMedicalPrescriptionId()
        );
        return ResponseEntity.ok(updatedVisit);
    }

    /**
     * The function deletes a visit by id
     *
     * @param id The id of the visit to delete.
     * @return A response entity with no content.
     */
    @DeleteMapping("/visit/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        visitService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
