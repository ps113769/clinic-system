package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.User;
import org.byleco.api.payload.JwtAuthenticationResponse;
import org.byleco.api.payload.LoginRequest;
import org.byleco.api.security.CurrentUser;
import org.byleco.api.security.JwtTokenProvider;
import org.byleco.api.security.UserPrincipal;
import org.byleco.api.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider tokenProvider;
    private final UserService userService;

    /**
     * It returns the user object of the currently logged in user
     *
     * @param userPrincipal The userPrincipal object is the object that contains the user's information.
     * @return A ResponseEntity object is being returned.
     */
    @GetMapping("/me")
    public ResponseEntity<User> me(@CurrentUser UserPrincipal userPrincipal) {
        return ResponseEntity.ok(userService.getUserFromPrincipal(userPrincipal));
    }

    /**
     * The function takes in a username and password, and returns a JWT token if the username and password are valid
     *
     * @param loginRequest This is the object that contains the username and password that the user entered.
     * @return A JWT token
     */
    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

}
