package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.MedicalOrder;
import org.byleco.api.payload.medicalOrder.MedicalOrderRequest;
import org.byleco.api.service.MedicalOrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class MedicalOrderController {
    private final MedicalOrderService medicalOrderService;

    /**
     * This function returns a list of all medical orders
     *
     * @return A list of medical orders.
     */
    @GetMapping("/medical-orders")
    public ResponseEntity<List<MedicalOrder>> getMedicalOrders() {
        List<MedicalOrder> medicalOrders = medicalOrderService.getMedicalOrders();
        return ResponseEntity.ok(medicalOrders);
    }

    /**
     * > This function fetches a medical order by id
     *
     * @param id The id of the medical order to fetch.
     * @return A ResponseEntity object is being returned.
     */
    @GetMapping("/medical-order/{id}")
    public ResponseEntity<MedicalOrder> fetchMedicalOrder(@PathVariable Long id) {
        MedicalOrder medicalOrder = medicalOrderService.fetchMedicalOrder(id);

        return ResponseEntity.ok(medicalOrder);
    }

    /**
     * It takes a MedicalOrderRequest object, validates it, and then creates a MedicalOrder object
     *
     * @param medicalOrderRequest This is the request body that is sent to the server.
     * @return A response entity with a medical order object.
     */
    @PostMapping("/medical-order")
    public ResponseEntity<MedicalOrder> createMedicalOrder(@RequestBody @Valid MedicalOrderRequest medicalOrderRequest) {
        MedicalOrder medicalOrder = medicalOrderService.createMedicalOrder(medicalOrderRequest);
        return ResponseEntity.ok(medicalOrder);
    }

    /**
     * It updates a medical order.
     *
     * @param id The id of the medical order to be updated.
     * @param medicalOrderRequest This is the request body that contains the data that will be used to update the medical
     * order.
     * @return A ResponseEntity with the updated MedicalOrder.
     */
    @PutMapping("/medical-order/{id}")
    public ResponseEntity<MedicalOrder> updateMedicalOrder(@PathVariable Long id, @RequestBody @Valid MedicalOrderRequest medicalOrderRequest) {
        MedicalOrder updatedMedicalOrder = medicalOrderService.updateMedicalOrder(id, medicalOrderRequest);
        return ResponseEntity.ok(updatedMedicalOrder);
    }

    /**
     * This function deletes a medical order by id
     *
     * @param id The id of the medical order to be deleted.
     * @return ResponseEntity.noContent().build();
     */
    @DeleteMapping("/medical-order/{id}")
    public ResponseEntity<Void> deleteMedicalOrder(@PathVariable Long id) {
        medicalOrderService.deleteMedicalOrder(id);
        return ResponseEntity.noContent().build();
    }
}
