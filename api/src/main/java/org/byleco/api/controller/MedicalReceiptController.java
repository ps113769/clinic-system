package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.MedicalReceipt;
import org.byleco.api.payload.medicalReceipt.CreateMedicalReceiptRequest;
import org.byleco.api.service.MedicalReceiptService;
import org.byleco.api.service.PdfGenerator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class MedicalReceiptController {
    private final MedicalReceiptService medicalReceiptService;
    private final PdfGenerator pdfGenerator;


    @GetMapping("/medical-receipts")
    public ResponseEntity<List<MedicalReceipt>> getMedicalReceipts() {
        List<MedicalReceipt> medicalreceipts = medicalReceiptService.getMedicalReceipts();
        return ResponseEntity.ok(medicalreceipts);
    }

    /**
     * > This function fetches a medical receipt by its id
     *
     * @param id The id of the medical receipt to fetch.
     * @return A ResponseEntity object is being returned.
     */
    @GetMapping("/medical-receipt/{id}")
    public ResponseEntity<MedicalReceipt> fetch(@PathVariable Long id) {
        MedicalReceipt medicalReceipt = medicalReceiptService.fetch(id);
        return ResponseEntity.ok(medicalReceipt);
    }

    /**
     * It takes a medical receipt id, fetches the receipt from the database, generates a pdf file from the receipt and
     * returns the path of the pdf file
     *
     * @param id The id of the medical receipt to be generated
     * @return The path of the generated pdf file.
     */
    @GetMapping("/medical-receipt/generate/{id}")
    public ResponseEntity<String> generateReceipt(@PathVariable Long id) throws Exception {
        MedicalReceipt medicalReceipt = medicalReceiptService.fetch(id);

        String path = pdfGenerator.GeneratePdf(medicalReceipt);

        return ResponseEntity.ok(path);
    }

    /**
     * > This function fetches all medical receipts of a patient with the given patient id
     *
     * @param patientId The id of the patient whose medical receipts you want to fetch.
     * @return A list of medical receipts
     */
    @GetMapping("/medical-receipt/patient/{patientId}")
    public ResponseEntity<List<MedicalReceipt>> fetchByPatientId(@PathVariable Long patientId) {
        List<MedicalReceipt> medicalReceipts = medicalReceiptService.fetchByPatientId(patientId);
        return ResponseEntity.ok(medicalReceipts);
    }

    /**
     * > This function creates a new medical receipt for a patient, given a list of drug IDs and an expiration date
     *
     * @param body The request body.
     * @return A response entity with a status of created and the body of the new medical receipt.
     */
    @PostMapping("/medical-receipt")
    public ResponseEntity<MedicalReceipt> create(@RequestBody CreateMedicalReceiptRequest body) {
        MedicalReceipt newMedicalReceipt = medicalReceiptService.create(
                body.getPatientId(),
                body.getDrugIds(),
                body.getExpirationDate()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(newMedicalReceipt);
    }

    /**
     * > Update a medical receipt with the given id, with the given patient id, drug ids, and expiration date
     *
     * @param id The id of the medical receipt to be updated
     * @param body The request body, containing all the fields.
     * @return A ResponseEntity object is being returned.
     */
    @PutMapping("/medical-receipt/{id}")
    public ResponseEntity<MedicalReceipt> update(@PathVariable Long id, @RequestBody CreateMedicalReceiptRequest body) {
        MedicalReceipt updatedMedicalReceipt = medicalReceiptService.update(
                id,
                body.getPatientId(),
                body.getDrugIds(),
                body.getExpirationDate()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(updatedMedicalReceipt);
    }
}