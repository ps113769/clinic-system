package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Patient;
import org.byleco.api.payload.patient.PatientRequest;
import org.byleco.api.payload.patient.PatientResponse;
import org.byleco.api.service.PatientService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * Controller for patient entity
 *
 * @author Adam Orlowski
 * @see <a href="https://github.com/OrlowskiAdam">Github</a>
 * @since 1.0
 * @version 1.0
 */
@RestController
@RequiredArgsConstructor
//@Secured({ "ROLE_ADMIN" })
@RequestMapping("/api")
public class PatientController {
    private final PatientService patientService;

    /**
     * Returns a list of patients
     *
     * @return A list of patients
     */
    @GetMapping("/patients")
    public ResponseEntity<List<Patient>> getPatients() {
        List<Patient> patients = patientService.getPatients();
        return ResponseEntity.ok(patients);
    }

    /**
     * It returns a list of patients with their full name
     *
     * @return A list of patients with their full name.
     */
    @GetMapping("/patients/fullname")
    public ResponseEntity<List<PatientResponse>> getPatientsFullName() {
        List<PatientResponse> patients = patientService.getPatientsFullName();
        return ResponseEntity.ok(patients);
    }

    /**
     * The function takes a patientId as a path variable, calls the patientService to get the patient, and returns the
     * patient
     *
     * @param patientId The id of the patient to retrieve.
     * @return A patient object
     */
    @GetMapping("/patient/{patientId}")
    public ResponseEntity<Patient> getPatient(@PathVariable Long patientId) {
        Patient patient = patientService.getPatient(patientId);
        return ResponseEntity.ok(patient);
    }

    /**
     * It takes a patientId and a request object, validates the request object, and then updates the patient with the given
     * patientId with the data in the request object
     *
     * @param patientId The id of the patient to be updated.
     * @param request The request body is the JSON object that is sent to the server.
     * @return A ResponseEntity is being returned.
     */
    @PutMapping("/patient/{patientId}")
    public ResponseEntity<Patient> updatePatient(@PathVariable Long patientId, @Valid @RequestBody PatientRequest request) {
        Patient patient = patientService.updatePatient(patientId, request);
        return ResponseEntity.ok(patient);
    }

    /**
     * This function takes a PatientRequest object, validates it, and then creates a new Patient object
     *
     * @param body The body of the request.
     * @return A ResponseEntity object is being returned.
     */
    @PostMapping("/patient")
    public ResponseEntity<Patient> createPatient(@RequestBody @Valid PatientRequest body) {
        Patient newPatient = patientService.createPatient(body);
        return ResponseEntity.ok(newPatient);
    }

    /**
     * It deletes a patient from the database
     *
     * @param patientId The id of the patient to be deleted.
     * @return ResponseEntity.noContent().build();
     */
    @DeleteMapping("/patient/{patientId}")
    public ResponseEntity<Void> deletePatient(@PathVariable Long patientId) {
        patientService.deletePatient(patientId);
        return ResponseEntity.noContent().build();
    }
}
