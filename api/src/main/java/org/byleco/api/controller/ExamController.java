package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Examination;
import org.byleco.api.payload.exam.ExamRequest;
import org.byleco.api.service.ExaminationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
//@Secured({ "ROLE_ADMIN" })
@RequestMapping("/api")
public class ExamController {
    private final ExaminationService examService;

    /**
     * Returns a list of exams
     *
     * @return A list of exams
     */
    @GetMapping("/exams")
    public ResponseEntity<List<Examination>> getExams() {
        List<Examination> exams = examService.getExams();
        return ResponseEntity.ok(exams);
    }

    /**
     * It returns a ResponseEntity of type Examination, which is a Spring class that wraps the HTTP response that will be
     * sent back to the client
     *
     * @param examId The id of the exam you want to get.
     * @return A ResponseEntity object is being returned.
     */
    @GetMapping("/exam/{examId}")
    public ResponseEntity<Examination> getExam(@PathVariable Long examId) {
        Examination exam = examService.getExam(examId);
        return ResponseEntity.ok(exam);
    }

    /**
     * It takes an examId and an ExamRequest object, and returns an Examination object
     *
     * @param examId The id of the exam you want to update.
     * @param request The request body is the exam object that we want to update.
     * @return ResponseEntity<Examination>
     */
    @PutMapping("/exam/{examId}")
    public ResponseEntity<Examination> updateExam(@PathVariable Long examId, @Valid @RequestBody ExamRequest request) {
        Examination exam = examService.updateExam(examId, request);
        return ResponseEntity.ok(exam);
    }

    /**
     * It takes a request body, validates it, and then creates an exam
     *
     * @param body The request body, which is a JSON object.
     * @return Examination object
     */
    @PostMapping("/exam")
    public ResponseEntity<Examination> createExam(@RequestBody @Valid ExamRequest body) {
        Examination newExam = examService.createExam(body);
        return ResponseEntity.ok(newExam);
    }

    /**
     * It deletes a exam from the database
     *
     * @param examId The id of the exam to be deleted.
     * @return ResponseEntity.noContent().build();
     */
    @DeleteMapping("/exam/{examId}")
    public ResponseEntity<Void> deleteExam(@PathVariable Long examId) {
        examService.deleteExam(examId);
        return ResponseEntity.noContent().build();
    }
}

