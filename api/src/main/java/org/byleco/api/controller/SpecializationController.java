package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.MedicalDepartment;
import org.byleco.api.model.Specialization;
import org.byleco.api.payload.specialization.SpecializationsByNameRequest;
import org.byleco.api.service.DoctorService;
import org.byleco.api.service.SpecializationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class SpecializationController {
    private final SpecializationService specializationService;

    /**
     * It returns a list of all specializations in the database
     *
     * @return A list of specializations
     */
    @GetMapping("/specializations")
    public ResponseEntity<List<Specialization>> getSpecializations() {
        List<Specialization> specializations = specializationService.getSpecializations();
        return ResponseEntity.ok(specializations);
    }
    /**
     * This function is used to get a specialization by its id
     *
     * @param id The id of the specialization you want to get.
     * @return A specialization object
     */
    @GetMapping("/specialization/{id}")
    public ResponseEntity<Specialization> getSpecializationByName(@PathVariable Long id) {
        Specialization specialization = specializationService.getById(id);
        return ResponseEntity.ok(specialization);
    }

    /**
     * > This function returns a list of specializations based on the names of the specializations passed in the request
     * body
     *
     * @param body The request body.
     * @return A list of specializations
     */
    @GetMapping("/specializations/by-name")
    public ResponseEntity<List<Specialization>> getSpecializationsByName(@RequestBody SpecializationsByNameRequest body) {
        List<Specialization> specializations = specializationService.findAllByName(body.getSpecializationNames());
        return ResponseEntity.ok(specializations);
    }
}
