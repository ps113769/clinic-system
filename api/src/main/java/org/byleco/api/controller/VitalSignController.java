package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.VitalSign;
import org.byleco.api.payload.vital.VitalSignRequest;
import org.byleco.api.service.VitalSignService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class VitalSignController {
    private final VitalSignService vitalSignService;

    /**
     * This function returns a list of vital signs
     *
     * @return A list of vital signs
     */
    @GetMapping("/vital-signs")
    public ResponseEntity<List<VitalSign>> getVitalSigns() {
        List<VitalSign> vitalSigns = vitalSignService.getVitalSigns();
        return ResponseEntity.ok(vitalSigns);
    }

    /**
     * It fetches a vital sign by its id
     *
     * @param vitalSignId The id of the vital sign to fetch.
     * @return A ResponseEntity object is being returned.
     */
    @GetMapping("/vital-sign/{vitalSignId}")
    public ResponseEntity<VitalSign> fetchVitalSign(@PathVariable Long vitalSignId) {
        VitalSign vitalSign = vitalSignService.fetchVitalSign(vitalSignId);

        return ResponseEntity.ok(vitalSign);
    }

    /**
     * It takes a `VitalSignRequest` object, validates it, and then creates a `VitalSign` object from it
     *
     * @param vitalSign This is the object that will be created.
     * @return A ResponseEntity object is being returned.
     */
    @PostMapping("/vital-sign")
    public ResponseEntity<VitalSign> createVitalSign(@RequestBody @Valid VitalSignRequest vitalSign) {
        VitalSign newVitalSign = vitalSignService.createVitalSign(vitalSign);
        return ResponseEntity.ok(newVitalSign);
    }

    /**
     * It takes a vitalSignId and a vitalSignRequest and updates the vitalSign with the given vitalSignId with the values
     * from the vitalSignRequest
     *
     * @param vitalSignId The ID of the vital sign to update.
     * @param vitalSign The request body, which is a JSON object that contains the vital sign data.
     * @return A ResponseEntity with the updated VitalSign object.
     */
    @PutMapping("/vital-sign/{vitalSignId}")
    public ResponseEntity<VitalSign> updateVitalSign(@PathVariable Long vitalSignId, @RequestBody @Valid VitalSignRequest vitalSign) {
        VitalSign updatedVitalSign = vitalSignService.updateVitalSign(vitalSignId, vitalSign);
        return ResponseEntity.ok(updatedVitalSign);
    }

    /**
     * This function deletes a vital sign from the database
     *
     * @param vitalSignId The id of the vital sign to be deleted.
     * @return A response entity with no content.
     */
    @DeleteMapping("/vital-sign/{vitalSignId}")
    public ResponseEntity<Void> deleteVitalSign(@PathVariable Long vitalSignId) {
        vitalSignService.deleteVitalSign(vitalSignId);
        return ResponseEntity.noContent().build();
    }
}
