package org.byleco.api.controller;

import lombok.RequiredArgsConstructor;
import org.byleco.api.model.Treatment;
import org.byleco.api.payload.treatment.CreateTreatmentRequest;
import org.byleco.api.service.TreatmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TreatmentController {
    private final TreatmentService treatmentService;

    /**
     * The function is a GET request that returns a list of treatments
     *
     * @return A list of treatments
     */
    @GetMapping("/treatments")
    public ResponseEntity<List<Treatment>> getTreatments() {
        List<Treatment> treatments = treatmentService.getTreatments();
        return ResponseEntity.ok(treatments);
    }

    /**
     * This function is a GET request that takes in a Long id and returns a ResponseEntity of type Treatment
     *
     * @param id The id of the treatment you want to get.
     * @return A treatment object
     */
    @GetMapping("/treatment/{id}")
    public ResponseEntity<Treatment> getTreatmentsById(@PathVariable Long id) {
        Treatment treatment = treatmentService.getById(id);
        return ResponseEntity.ok(treatment);
    }

    /**
     * It creates a new treatment
     *
     * @param body The request body, which is a JSON object.
     * @return ResponseEntity<Treatment>
     */
    @PostMapping("/treatment")
    public ResponseEntity<Treatment> createTreatment(@RequestBody CreateTreatmentRequest body) {
        Treatment newTreatment = treatmentService.create(
                body.getDate(),
                body.getMedicalInterview(),
                body.getDiagnosis(),
                body.getPatientId(),
                body.getDoctorId(),
                body.getExaminationsId(),
                body.getVitalSignsId(),
                body.getNumberOfRoom()

        );
        return ResponseEntity.status(HttpStatus.CREATED).body(newTreatment);
    }

    /**
     * It updates a treatment with the given id, using the data from the body of the request
     *
     * @param id The id of the treatment to be updated.
     * @param body The request body, containing all the fields required to create a new treatment.
     * @return ResponseEntity<Treatment>
     */
    @PutMapping("/treatment/{id}")
    public ResponseEntity<Treatment> updateTreatment(@PathVariable Long id, @RequestBody CreateTreatmentRequest body) {
        Treatment updatedTreatment = treatmentService.update(
                id,
                body.getDate(),
                body.getMedicalInterview(),
                body.getDiagnosis(),
                body.getPatientId(),
                body.getDoctorId(),
                body.getExaminationsId(),
                body.getVitalSignsId(),
                body.getNumberOfRoom()
        );
        return ResponseEntity.ok(updatedTreatment);
    }
}
