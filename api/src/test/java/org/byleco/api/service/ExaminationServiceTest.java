package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Examination;
import org.byleco.api.model.Patient;
import org.byleco.api.payload.exam.ExamRequest;
import org.byleco.api.repository.ExaminationRepository;
import org.byleco.api.repository.PatientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExaminationServiceTest {
    @Mock
    ExaminationRepository examRepository;

    @Mock
    PatientRepository patientRepository;

    @InjectMocks
    ExaminationService examinationService;

    ExamRequest examRequest;
    Patient patient;
    Examination examination;

    @BeforeEach
    void setUp() {
        examinationService = new ExaminationService(examRepository, patientRepository);
        examRequest = ExamRequest.builder()
                .name("test")
                .description("test")
                .result("test")
                .patient_id(1L)
                .date(LocalDate.now())
                .realized(false)
                .build();
        patient = Patient.builder()
                .id(1L)
                .name("Marek")
                .build();
        examination = Examination.builder()
                .id(10L)
                .name(examRequest.getName())
                .description(examRequest.getDescription())
                .date(examRequest.getDate())
                .patient(patient)
                .result(examRequest.getResult())
                .realized(examRequest.getRealized())
                .build();
    }

    @Test
    void shouldCallFindAllMethodFromExaminationRepository() {
        // when
        when(examRepository.findAll()).thenReturn(new ArrayList<>());
        List<Examination> exams = examinationService.getExams();
        // then
        verify(examRepository, times(1)).findAll();
        assertEquals(exams, new ArrayList<>());
    }

    @Test
    void shouldCreateExamInRepositoryWhenCallCreateExam() {
        // when
        when(patientRepository.findById(1L)).thenReturn(Optional.of(patient));
        when(examRepository.save(any())).thenReturn(examination);
        Examination exam = examinationService.createExam(examRequest);
        // then
        assertEquals(exam, examination);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenPatientIsNotFoundDuringExamCreation() {
        // then
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> examinationService.createExam(examRequest));
        assertEquals("Patient not found", exception.getMessage());
    }

    @Test
    void deleteExam() {
        examinationService.deleteExam(anyLong());
        verify(examRepository).deleteById(anyLong());
    }

    @Test
    void getExam() {
        // when
        when(examRepository.findById(10L)).thenReturn(Optional.of(examination));
        // then
        Examination exam = examinationService.getExam(10L);
        assertEquals(exam, examination);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenPatientIsNotFoundDuringExamUpdate() {
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> examinationService.updateExam(anyLong(), examRequest));
        assertEquals("Patient not found", exception.getMessage());
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenExaminationIsNotFoundDuringExamUpdate() {
        // when
        when(patientRepository.findById(1L)).thenReturn(Optional.of(patient));
        // then
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> examinationService.updateExam(anyLong(), examRequest));
        assertEquals("Exam not found", exception.getMessage());
    }

    @Test
    void shouldUpdateExaminationWhenPatientAndExaminationIsFound() {
        // when
        when(patientRepository.findById(1L)).thenReturn(Optional.of(patient));
        when(examRepository.findById(10L)).thenReturn(Optional.of(examination));
        when(examRepository.save(any())).thenReturn(examination);
        // then
        Examination exam = examinationService.updateExam(10L, examRequest);
        assertEquals(examination, exam);
    }
}