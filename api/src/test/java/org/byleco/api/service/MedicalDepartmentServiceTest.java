package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.MedicalDepartment;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicalDepartmentServiceTest {
    @Mock
    MedicalDepartmentRepository medicalDepartmentRepository;

    @InjectMocks
    MedicalDepartmentService medicalDepartmentService;

    @BeforeEach
    void setUp() {
        medicalDepartmentService = new MedicalDepartmentService(medicalDepartmentRepository);
    }

    @Test
    void shouldCallFindAllMethodFromMedicalDepartmentRepository() {
        // when
        when(medicalDepartmentRepository.findAll()).thenReturn(new ArrayList<>());
        List<MedicalDepartment> medicalDepartments = medicalDepartmentService.getMedicalDepartaments();
        // then
        verify(medicalDepartmentRepository).findAll();
        assertEquals(medicalDepartments, new ArrayList<>());
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenMedicalDepartmentIsNotFound() {
        // then
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> medicalDepartmentService.getById(anyLong()));
        assertEquals("Department not found", exception.getMessage());
    }

    @Test
    void shouldCallFindByIdMethodFromMedicalDepartmentRepository() {
        // given
        MedicalDepartment medicalDepartment = MedicalDepartment.builder()
                .id(1L)
                .name("test")
                .build();
        // when
        when(medicalDepartmentRepository.findById(anyLong())).thenReturn(Optional.of(medicalDepartment));
        MedicalDepartment foundMedicalDepartment = medicalDepartmentService.getById(1L);
        // then
        assertEquals(medicalDepartment, foundMedicalDepartment);
    }
}