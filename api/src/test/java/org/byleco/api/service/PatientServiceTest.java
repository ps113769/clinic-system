package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Patient;
import org.byleco.api.payload.patient.PatientRequest;
import org.byleco.api.payload.patient.PatientResponse;
import org.byleco.api.repository.PatientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PatientServiceTest {
    @Mock
    PatientRepository patientRepository;

    @InjectMocks
    PatientService patientService;

    PatientResponse patientResponse1;
    PatientResponse patientResponse2;
    Patient patient;
    List<Patient> patients;
    PatientRequest patientRequest;

    @BeforeEach
    void setUp() {
        patientService = new PatientService(patientRepository);
        patientResponse1 = PatientResponse.builder()
                .id(1L)
                .name("Marek")
                .surname("Byleco")
                .phone("123456789")
                .address("ul. Byleco")
                .city("Bydgoszcz")
                .state("Bydgoszcz")
                .zip("00-000")
                .country("Poland")
                .fullName("Marek Byleco")
                .build();
        patientResponse2 = PatientResponse.builder()
                .id(2L)
                .name("Marek")
                .surname("Byleco")
                .phone("123456789")
                .address("ul. Byleco")
                .city("Bydgoszcz")
                .state("Bydgoszcz")
                .zip("00-000")
                .country("Poland")
                .fullName("Marek Byleco")
                .build();
        patient = Patient.builder()
                .id(1L)
                .name("Marek")
                .surname("Byleco")
                .phone("123456789")
                .address("ul. Byleco")
                .city("Bydgoszcz")
                .state("Bydgoszcz")
                .zip("00-000")
                .country("Poland")
                .build();
        patients = List.of(patient);
        patientRequest = PatientRequest.builder()
                .name("Marek")
                .surname("Byleco")
                .phone("123456789")
                .address("ul. Byleco")
                .city("Bydgoszcz")
                .state("Bydgoszcz")
                .zip("00-000")
                .country("Poland")
                .build();
    }

    @Test
    void shouldCallFindAllMethodFromPatientRepository() {
        // when
        when(patientRepository.findAll()).thenReturn(new ArrayList<>());
        List<Patient> patients = patientService.getPatients();
        // then
        verify(patientRepository).findAll();
        assertEquals(0, patients.size());
        assertEquals(patients, new ArrayList<>());
    }

    @Test
    void shouldReturnPatientResponseWhenCallingForPatientsFullName() {
        // when
        when(patientRepository.findAll()).thenReturn(patients);
        // then
        List<PatientResponse> patientResponses = patientService.getPatientsFullName();
        assertEquals(1, patientResponses.size());
        assertEquals(patientResponse1.getId(), patientResponses.get(0).getId());
        assertEquals(patientResponse1.getName(), patientResponses.get(0).getName());
        assertEquals(patientResponse1.getSurname(), patientResponses.get(0).getSurname());
        assertEquals(patientResponse1.getPhone(), patientResponses.get(0).getPhone());
        assertEquals(patientResponse1.getAddress(), patientResponses.get(0).getAddress());
        assertEquals(patientResponse1.getCity(), patientResponses.get(0).getCity());
        assertEquals(patientResponse1.getState(), patientResponses.get(0).getState());
        assertEquals(patientResponse1.getZip(), patientResponses.get(0).getZip());
        assertEquals(patientResponse1.getCountry(), patientResponses.get(0).getCountry());
        assertEquals(patientResponse1.getFullName(), patientResponses.get(0).getFullName());
    }

    @Test
    void shouldReturnEmptyPatientResponseWhenNoDataIsPresent() {
        // when
        when(patientRepository.findAll()).thenReturn(new ArrayList<>());
        // then
        List<PatientResponse> patientResponses = patientService.getPatientsFullName();
        assertEquals(0, patientResponses.size());
    }

    @Test
    void shouldCallSaveMethodWhenCreatingPatient() {
        // when
        when(patientRepository.save(any(Patient.class))).thenReturn(this.patient);
        // then
        Patient patient = patientService.createPatient(patientRequest);
        verify(patientRepository).save(any(Patient.class));
        assertEquals(patient.getId(), patientResponse1.getId());
        assertEquals(patient.getName(), patientResponse1.getName());
        assertEquals(patient.getSurname(), patientResponse1.getSurname());
        assertEquals(patient.getPhone(), patientResponse1.getPhone());
        assertEquals(patient.getAddress(), patientResponse1.getAddress());
        assertEquals(patient.getCity(), patientResponse1.getCity());
        assertEquals(patient.getState(), patientResponse1.getState());
        assertEquals(patient.getZip(), patientResponse1.getZip());
        assertEquals(patient.getCountry(), patientResponse1.getCountry());
    }

    @Test
    void shouldCallDeleteByIdMethodFromPatientRepository() {
        patientService.deletePatient(anyLong());
        verify(patientRepository).deleteById(anyLong());
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenPatientIsNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> patientService.getPatient(anyLong()));
    }

    @Test
    void shouldFindPatientWithProvidedId() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        // then
        Patient patient = patientService.getPatient(anyLong());
        assertEquals(patient, this.patient);
    }

    @Test
    void shouldUpdatePatient() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(patientRepository.save(any(Patient.class))).thenReturn(patient);
        // then
        Patient patient = patientService.updatePatient(anyLong(), patientRequest);
        assertEquals(patient, this.patient);
    }
}