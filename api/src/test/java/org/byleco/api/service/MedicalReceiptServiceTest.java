package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Drug;
import org.byleco.api.model.MedicalReceipt;
import org.byleco.api.model.Patient;
import org.byleco.api.repository.DrugRepository;
import org.byleco.api.repository.MedicalReceiptRepository;
import org.byleco.api.repository.PatientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicalReceiptServiceTest {
    @Mock
    MedicalReceiptRepository medicalReceiptRepository;

    @Mock
    DrugRepository drugRepository;

    @Mock
    PatientRepository patientRepository;

    @InjectMocks
    MedicalReceiptService medicalReceiptService;

    MedicalReceipt medicalReceipt;
    Patient patient;
    Drug drug1;
    Drug drug2;
    List<Drug> drugs;

    @BeforeEach
    void setUp() {
        medicalReceiptService = new MedicalReceiptService(medicalReceiptRepository, drugRepository, patientRepository);
        patient = Patient.builder()
                .id(1L)
                .name("Marek")
                .build();
        drug1 = Drug.builder()
                .id(1L)
                .name("test")
                .build();
        drug2 = Drug.builder()
                .id(2L)
                .name("test2")
                .build();
        drugs = List.of(drug1, drug2);
        medicalReceipt = MedicalReceipt.builder()
                .id(1L)
                .patient(patient)
                .expirationDate(LocalDate.now())
                .drugs(drugs)
                .build();
    }

    @Test
    void shouldCallFindAllMethodFromMedicalReceiptRepository() {
        // when
        when(medicalReceiptRepository.findAll()).thenReturn(new ArrayList<>());
        // then
        List<MedicalReceipt> medicalReceipts = medicalReceiptService.getMedicalReceipts();
        verify(medicalReceiptRepository).findAll();
        assertEquals(medicalReceipts, new ArrayList<>());
    }

    @Test
    void shouldCallFindByIdMethodFromMedicalReceiptRepository() {
        // when
        when(medicalReceiptRepository.findById(anyLong())).thenReturn(Optional.of(medicalReceipt));
        // then
        MedicalReceipt medicalReceipt = medicalReceiptService.fetch(anyLong());
        assertEquals(medicalReceipt, this.medicalReceipt);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenCallFindByIdMethodFromMedicalReceiptRepository() {
        // when
        when(medicalReceiptRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalReceiptService.fetch(anyLong()));
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenCreatingMedicalReceiptAndPatientIsNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalReceiptService.create(anyLong(), new ArrayList<>(), null));
    }

    @Test
    void shouldCreateMedicalReceipt() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(drugRepository.findAllById(List.of(1L, 2L))).thenReturn(drugs);
        when(medicalReceiptRepository.save(any(MedicalReceipt.class))).thenReturn(medicalReceipt);
        // then
        MedicalReceipt medicalReceipt = medicalReceiptService.create(anyLong(), List.of(1L, 2L), LocalDate.now());
        assertEquals(medicalReceipt.getPatient(), patient);
        assertEquals(medicalReceipt.getDrugs(), drugs);
        assertEquals(medicalReceipt, this.medicalReceipt);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenUpdatingMedicalReceiptAndPatientIsNotFound() {
        // when
        when(medicalReceiptRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> medicalReceiptService.update(anyLong(), null, null, null));
        assertThrows(ResourceNotFoundException.class, () -> medicalReceiptService.update(anyLong(), null, null, null));
        assertEquals(exception.getMessage(), "Receipt not found");
    }

    @Test
    void shouldUpdateMedicalReceipt() {
        // when
        when(medicalReceiptRepository.findById(anyLong())).thenReturn(Optional.of(medicalReceipt));
        when(patientRepository.findById(1L)).thenReturn(Optional.of(patient));
        when(drugRepository.findAllById(List.of(1L, 2L))).thenReturn(drugs);
        when(medicalReceiptRepository.save(any(MedicalReceipt.class))).thenReturn(medicalReceipt);
        // then
        MedicalReceipt medicalReceipt = medicalReceiptService.update(anyLong(), 1L, List.of(1L, 2L), LocalDate.now());
        assertEquals(medicalReceipt.getPatient(), patient);
        assertEquals(medicalReceipt.getDrugs(), drugs);
        assertEquals(medicalReceipt, this.medicalReceipt);
    }

}