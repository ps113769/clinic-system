package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.MedicalOrder;
import org.byleco.api.model.Nurse;
import org.byleco.api.model.Patient;
import org.byleco.api.payload.medicalOrder.MedicalOrderRequest;
import org.byleco.api.repository.MedicalOrderRepository;
import org.byleco.api.repository.NurseRepository;
import org.byleco.api.repository.PatientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MedicalOrderServiceTest {
    @Mock
    MedicalOrderRepository medicalOrderRepository;

    @Mock
    PatientRepository patientRepository;

    @Mock
    NurseRepository nurseRepository;

    @InjectMocks
    MedicalOrderService medicalOrderService;

    MedicalOrder medicalOrder;
    Patient patient;
    Nurse nurse1;
    Nurse nurse2;
    List<Nurse> nurses;
    MedicalOrderRequest medicalOrderRequest;

    @BeforeEach
    void setUp() {
        medicalOrderService = new MedicalOrderService(medicalOrderRepository, patientRepository, nurseRepository);
        patient = Patient.builder()
                .id(1L)
                .name("Marek")
                .build();
        nurse1 = Nurse.builder()
                .id(1L)
                .firstName("Marek")
                .lastName("Byleco")
                .build();
        nurse2 = Nurse.builder()
                .id(2L)
                .firstName("Kamil")
                .lastName("Nowak")
                .build();
        nurses = List.of(nurse1, nurse2);
        medicalOrder = MedicalOrder.builder()
                .id(1L)
                .name("test")
                .description("test")
                .patient(patient)
                .nurse(nurses)
                .build();
        medicalOrderRequest = MedicalOrderRequest.builder()
                .name("test")
                .description("test")
                .patient_id(1L)
                .nurses(List.of(1L, 2L))
                .build();
    }

    @Test
    void shouldCallFindAllMethodFromMedicalOrderRepository() {
        // when
        when(medicalOrderRepository.findAll()).thenReturn(new ArrayList<>());
        List<MedicalOrder> medicalOrders = medicalOrderService.getMedicalOrders();
        // then
        verify(medicalOrderRepository).findAll();
        assertEquals(medicalOrders, new ArrayList<>());
    }

    @Test
    void shouldCallFindByIdMethodFromMedicalOrderRepository() {
        // when
        when(medicalOrderRepository.findById(1L)).thenReturn(Optional.of(this.medicalOrder));
        MedicalOrder medicalOrder = medicalOrderService.fetchMedicalOrder(1L);
        // then
        verify(medicalOrderRepository).findById(1L);
        assertEquals(medicalOrder, this.medicalOrder);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenCallFindByIdMethodFromMedicalOrderRepository() {
        // when
        when(medicalOrderRepository.findById(1L)).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalOrderService.fetchMedicalOrder(1L));
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenCreatingMedicalOrderAndPatientIsNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalOrderService.createMedicalOrder(medicalOrderRequest));
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenCreatingMedicalOrderAndOneOfTheNursesAreNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(nurseRepository.findById(1L)).thenReturn(Optional.of(nurse1));
        when(nurseRepository.findById(2L)).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalOrderService.createMedicalOrder(medicalOrderRequest));
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenCreatingMedicalOrderAndNurseIsNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(nurseRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalOrderService.createMedicalOrder(medicalOrderRequest));
    }

    @Test
    void shouldCreateMedicalOrder() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(nurseRepository.findById(1L)).thenReturn(Optional.of(nurse1));
        when(nurseRepository.findById(2L)).thenReturn(Optional.of(nurse2));
        when(medicalOrderRepository.save(any(MedicalOrder.class))).thenReturn(medicalOrder);
        // then
        MedicalOrder medicalOrder = medicalOrderService.createMedicalOrder(medicalOrderRequest);
        // then
        verify(medicalOrderRepository).save(any(MedicalOrder.class));
        assertEquals(medicalOrder, this.medicalOrder);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenUpdatingMedicalOrderAndPatientIsNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalOrderService.updateMedicalOrder(anyLong(), medicalOrderRequest));
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenUpdatingMedicalOrderAndOneOfTheNursesAreNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(nurseRepository.findById(1L)).thenReturn(Optional.of(nurse1));
        when(nurseRepository.findById(2L)).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalOrderService.updateMedicalOrder(anyLong(), medicalOrderRequest));
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenUpdatingMedicalOrderAndNurseIsNotFound() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(nurseRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> medicalOrderService.updateMedicalOrder(anyLong(), medicalOrderRequest));
    }

    @Test
    void shouldUpdateMedicalOrder() {
        // when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        when(nurseRepository.findById(1L)).thenReturn(Optional.of(nurse1));
        when(nurseRepository.findById(2L)).thenReturn(Optional.of(nurse2));
        when(medicalOrderRepository.findById(anyLong())).thenReturn(Optional.of(medicalOrder));
        when(medicalOrderRepository.save(any(MedicalOrder.class))).thenReturn(medicalOrder);
        // then
        MedicalOrder medicalOrder = medicalOrderService.updateMedicalOrder(anyLong(), medicalOrderRequest);
        // then
        verify(medicalOrderRepository).save(any(MedicalOrder.class));
        assertEquals(medicalOrder, this.medicalOrder);
    }

    @Test
    void shouldCallDeleteByIdWhenCallingMedicalOrderRepositoryMethod() {
        medicalOrderService.deleteMedicalOrder(anyLong());
        verify(medicalOrderRepository).deleteById(anyLong());
    }
}