package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Drug;
import org.byleco.api.repository.DrugRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DrugServiceTest {
    @Mock
    DrugRepository drugRepository;

    @InjectMocks
    DrugService drugService;

    Drug drug;

    @BeforeEach
    void setUp() {
        drugService = new DrugService(drugRepository);
        drug = Drug.builder()
                .id(1L)
                .name("test")
                .build();
    }

    @Test
    void shouldCallFindAllMethodFromDrugRepository() {
        // when
        when(drugRepository.findAll()).thenReturn(new ArrayList<>());
        List<Drug> drugs = drugService.getDrugs();
        // then
        assertEquals(drugs, new ArrayList<>());
        verify(drugRepository).findAll();
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenDrugIsNotFound() {
        // then
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> drugService.getById(anyLong()));
        assertEquals("Drug not found", exception.getMessage());
        assertThrows(ResourceNotFoundException.class, () -> drugService.getById(anyLong()));
    }

    @Test
    void getById() {
        // when
        when(drugRepository.findById(anyLong())).thenReturn(Optional.of(drug));
        Drug drug1 = drugService.getById(1L);
        // then
        assertEquals(drug, drug1);
    }

    @Test
    void shouldFindAllDrugsByName() {
        // given
        Drug drug1 = Drug.builder()
                .id(1L)
                .name("test1")
                .build();
        Drug drug2 = Drug.builder()
                .id(2L)
                .name("test1")
                .build();
        // when
        when(drugRepository.findByName("test1")).thenReturn(Optional.of(drug1));
        when(drugRepository.findByName("test2")).thenReturn(Optional.of(drug2));
        List<Drug> drugs = drugService.findAllByName(List.of("test1", "test2"));
        // then
        assertEquals(drugs.size(), 2);
        assertEquals(drugs.get(0), drug1);
        assertEquals(drugs.get(1), drug2);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenDrugIsNotFoundWhenCallingFindAllByNameMethod() {
        // when
        when(drugRepository.findByName("test")).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> drugService.findAllByName(List.of("test")));
    }
}