package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.MedicalDepartment;
import org.byleco.api.model.MedicalOrder;
import org.byleco.api.model.Nurse;
import org.byleco.api.model.Patient;
import org.byleco.api.payload.nurse.NurseRequest;
import org.byleco.api.payload.nurse.NurseSelectListResponse;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.byleco.api.repository.NurseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NurseServiceTest {
    @Mock
    NurseRepository nurseRepository;

    @Mock
    MedicalDepartmentRepository medicalDepartmentRepository;

    @InjectMocks
    NurseService nurseService;

    Nurse nurse1;
    Nurse nurse2;
    List<Nurse> nurses;
    MedicalDepartment medicalDepartment;
    MedicalOrder medicalOrder1;
    MedicalOrder medicalOrder2;
    Patient patient;
    NurseSelectListResponse nurseSelectListResponse1;
    NurseSelectListResponse nurseSelectListResponse2;
    NurseRequest nurseRequest;

    @BeforeEach
    void setUp() {
        nurseService = new NurseService(nurseRepository, medicalDepartmentRepository);
        nurse1 = Nurse.builder()
                .id(1L)
                .firstName("Marek")
                .lastName("Byleco")
                .build();
        nurse2 = Nurse.builder()
                .id(2L)
                .firstName("Kamil")
                .lastName("Nowak")
                .build();
        nurses = List.of(nurse1, nurse2);
        patient = Patient.builder()
                .id(1L)
                .name("Marek")
                .build();
        medicalDepartment = MedicalDepartment.builder()
                .id(1L)
                .name("md")
                .build();
        medicalOrder1 = MedicalOrder.builder()
                .id(1L)
                .name("test")
                .description("test")
                .patient(patient)
                .nurse(nurses)
                .build();
        medicalOrder2 = MedicalOrder.builder()
                .id(2L)
                .name("test")
                .description("test")
                .patient(patient)
                .nurse(nurses)
                .build();
        nurseSelectListResponse1 = NurseSelectListResponse.builder()
                .id(1L)
                .fullName(nurse1.getFirstName() + " " + nurse1.getLastName())
                .build();
        nurseSelectListResponse2 = NurseSelectListResponse.builder()
                .id(2L)
                .fullName(nurse2.getFirstName() + " " + nurse2.getLastName())
                .build();
        nurseRequest = NurseRequest.builder()
                .firstName("test")
                .lastName("test")
                .medicalDepartment_id(1L)
                .build();
    }

    @Test
    void shouldReturnAllNurses() {
        // when
        when(nurseRepository.findAll()).thenReturn(new ArrayList<>());
        // then
        assertEquals(nurseService.getNurses(), new ArrayList<>());
    }

    @Test
    void shouldReturnListOfNurseSelect() {
        // when
        when(nurseRepository.findAll()).thenReturn(nurses);
        // then
        List<NurseSelectListResponse> nurseSelectListResponses = nurseService.getNursesSelectList();
        assertEquals(nurseSelectListResponses.size(), 2);
        assertEquals(nurseSelectListResponses.get(0).getFullName(), nurseSelectListResponse1.getFullName());
        assertEquals(nurseSelectListResponses.get(1).getFullName(), nurseSelectListResponse2.getFullName());
        assertEquals(nurseSelectListResponses.get(0).getId(), nurseSelectListResponse1.getId());
        assertEquals(nurseSelectListResponses.get(1).getId(), nurseSelectListResponse2.getId());
    }

    @Test
    void shouldReturnEmptyListOfNurseSelect() {
        // when
        when(nurseRepository.findAll()).thenReturn(new ArrayList<>());
        // then
        List<NurseSelectListResponse> nurseSelectListResponses = nurseService.getNursesSelectList();
        assertEquals(nurseSelectListResponses.size(), 0);
    }

    @Test
    void shouldCallFindByIdMethodFromNurseRepository() {
        // when
        when(nurseRepository.findById(anyLong())).thenReturn(Optional.of(nurse1));
        // then
        assertEquals(nurseService.fetchNurse(anyLong()), nurse1);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenNurseNotFound() {
        // when
        when(nurseRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> nurseService.fetchNurse(anyLong()));
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenMedicalDepartmentIsNotFoundDuringNurseCreation() {
        // when
        when(medicalDepartmentRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> nurseService.createNurse(nurseRequest));
    }

    @Test
    void shouldCallSaveMethodFromNurseRepositoryDuringNurseCreation() {
        // when
        when(medicalDepartmentRepository.findById(anyLong())).thenReturn(Optional.of(medicalDepartment));
        when(nurseRepository.save(any(Nurse.class))).thenReturn(nurse1);
        // then
        Nurse nurse = nurseService.createNurse(nurseRequest);
        assertEquals(nurse, nurse1);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenNurseIsNotFoundDuringNurseUpdate() {
        // when
        when(nurseRepository.findById(anyLong())).thenReturn(Optional.empty());
        // then
        assertThrows(ResourceNotFoundException.class, () -> nurseService.updateNurse(anyLong(), nurseRequest));
    }

    @Test
    void shouldCallSaveMethodFromNurseRepositoryDuringNurseUpdate() {
        // when
        when(nurseRepository.findById(anyLong())).thenReturn(Optional.of(nurse1));
        when(nurseRepository.save(any(Nurse.class))).thenReturn(nurse1);
        when(medicalDepartmentRepository.findById(anyLong())).thenReturn(Optional.of(medicalDepartment));
        // then
        Nurse nurse = nurseService.updateNurse(anyLong(), nurseRequest);
        assertEquals(nurse, nurse1);
    }

    @Test
    void shouldCallDeleteByIdMethodFromNurseRepositoryDuringNurseDeletion() {
        nurseService.deleteNurse(anyLong());
        verify(nurseRepository).deleteById(anyLong());
    }
}