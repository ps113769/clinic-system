package org.byleco.api.service;

import org.byleco.api.exception.ResourceNotFoundException;
import org.byleco.api.model.Doctor;
import org.byleco.api.model.MedicalDepartment;
import org.byleco.api.payload.doctor.DoctorRequest;
import org.byleco.api.repository.DoctorRepository;
import org.byleco.api.repository.MedicalDepartmentRepository;
import org.byleco.api.repository.SpecializationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DoctorServiceTest {
    @Mock
    DoctorRepository doctorRepository;

    @Mock
    MedicalDepartmentRepository medicalDepartmentRepository;

    @Mock
    SpecializationRepository specializationRepository;

    @InjectMocks
    DoctorService doctorService;

    Doctor doctor;
    DoctorRequest doctorRequest;
    MedicalDepartment medicalDepartment;

    @BeforeEach
    void setUp() {
        doctorService = new DoctorService(doctorRepository, medicalDepartmentRepository, specializationRepository);
        doctor = Doctor.builder()
                .id(1L)
                .firstName("Marek")
                .lastName("Kowalski")
                .build();
        doctorRequest = DoctorRequest.builder()
                .firstName("Marek")
                .lastName("Kowalski")
                .medicalDepartment_id(1L)
                .specializations(new ArrayList<>())
                .build();
        medicalDepartment = MedicalDepartment.builder()
                .id(1L)
                .name("test")
                .build();
    }

    @Test
    void shouldCallFindAllMethodFromDoctorRepository() {
        // when
        when(doctorRepository.findAll()).thenReturn(new ArrayList<>());
        List<Doctor> doctors = doctorRepository.findAll();
        // then
        verify(doctorRepository).findAll();
        assertEquals(doctors, new ArrayList<>());
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenDoctorNotFound() {
        // when
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> doctorService.fetchDoctor(anyLong()));
        assertEquals("Doctor not found", exception.getMessage());
    }

    @Test
    void shouldCallFindByIdMethodFromDoctorRepository() {
        // when
        when(doctorRepository.findById(1L)).thenReturn(Optional.of(doctor));
        Doctor doc = doctorService.fetchDoctor(1L);
        // then
        verify(doctorRepository).findById(1L);
        assertEquals(doc, doctor);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenMedicalDepartmentIsNotFoundDuringDoctorCreation() {
        // when
        when(medicalDepartmentRepository.findById(1L)).thenReturn(Optional.empty());
        // then
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> doctorService.createDoctor(doctorRequest));
        assertEquals("Medical Department not found", exception.getMessage());
    }

    @Test
    void shouldThrowResourceNotFoundExceptionWhenDoctorIsNotFoundDuringDoctorUpdate() {
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> doctorService.updateDoctor(anyLong(), doctorRequest));
        assertEquals("Doctor not found", exception.getMessage());
    }

    @Test
    void shouldCallSaveMethodFromDoctorRepositoryOnDoctorUpdate() {
        // when
        when(doctorRepository.findById(1L)).thenReturn(Optional.of(doctor));
        when(medicalDepartmentRepository.findById(1L)).thenReturn(Optional.of(medicalDepartment));
        when(doctorRepository.save(doctor)).thenReturn(doctor);
        Doctor doc = doctorService.updateDoctor(1L, doctorRequest);
        // then
        verify(doctorRepository).findById(1L);
        verify(doctorRepository).save(doctor);
        assertEquals(doc, doctor);
    }

    @Test
    void deleteDoctor() {
        doctorService.deleteDoctor(anyLong());
        verify(doctorRepository).deleteById(anyLong());
    }
}